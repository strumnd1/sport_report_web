# Sport Report #


### Quick Summary ###

This website is for the the Sport Report application used in tracking player wellness.This works in conjunction with 
iOS and Android apps of the same name. The site allows for coaches to track how well their players are doing in regards
to their fatigue, how much sleep they are getting, and how stressed they are. There is much graduate study into knowing
a team's overall wellness allows a coach to better prepare and work players. 

### Who do I talk to? ###

Contact strumsd1@gcc.edu with any questions.
Copyright of Grove City College
Build by Le Crew Studios