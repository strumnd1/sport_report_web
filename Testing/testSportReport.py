# Modified from: http://selenium-python.readthedocs.io/getting-started.html#using-selenium-to-write-tests

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time

#testing requirement: "As a registered user, I can log into a preexisting account."

#Preexisting account details:
#Username: coach@coach.com
#Password: password

def test_bad_email():
    driver = webdriver.Chrome("C:\\Users\\covattorh1\\Desktop\\chromedriver.exe")
    driver.get("http://127.0.0.1:50826/SignUp/index.html")
    
    # Check that the page is there
    assert "Sport Report" in driver.title
    
    # find email element and enter a faulty email
    # this email is faulty because it does not have a .com
    elem = driver.find_element_by_id("email")
    elem.send_keys("nick@whatever")
    time.sleep(5)
    
    elem2 = driver.find_element_by_id("password")
    #this is the default passwords for most accounts
    elem2.send_keys("password")
    time.sleep(5)
    
    elem3 = driver.find_element_by_id("loginButton")
    elem3.click()
    time.sleep(5)
    # if the email had bad format a message should appear displaying an error
    elem = driver.find_element_by_id("errorCode")
    assert "The email address is badly formatted." in elem.text
        
    # driver.close() is another common function.
    # It closes the current tab
    driver.quit()
    
def test_invalid_login():
    driver = webdriver.Chrome("C:\\Users\\covattorh1\\Desktop\\chromedriver.exe")
    driver.get("http://localhost/Picturesque/")
    
    # Check that the page is there
    assert "FBPhotoBox Demo" in driver.title
    
    # Search for "pycon"
    elem = driver.find_element_by_id("loginLogoutButton")
    elem.click()
    
    time.sleep(5)
    elem1 = driver.find_element_by_name("username")
    elem1.send_keys("sam")
    time.sleep(5)
    elem2 = driver.find_element_by_name("pass")
    elem2.send_keys("nick")
    time.sleep(5)
    elem3 = driver.find_element_by_name("login")
    elem3.click()
    time.sleep(5)
    
    alert = driver.switch_to.alert
    # if the user successfully logged in, he would see an alert screen
    assert "Username or password incorrect" in alert.text
        
    # driver.close() is another common function.
    # It closes the current tab
    driver.quit()
    
