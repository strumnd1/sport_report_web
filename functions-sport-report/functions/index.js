const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);

// Create and Deploy Your First Cloud Functions
// https://firebase.google.com/docs/functions/write-firebase-functions
//https://github.com/firebase/functions-samples for examples


exports.findTeamAverages = functions.database.ref('/teams/{teamID}/wellnessData/{dateID}/wellnessAnswered').onWrite(event => {
      
      // Grab the current value of what was written to the Realtime Database.
      let snapshot = event.data.ref.parent;
      return snapshot.once('value').then(DataSnapshot => {
        if(DataSnapshot.val() == null){
            return null;
        }
        var fatigueSum = 0;
        var sorenessSum = 0;
        var moodSum = 0;
        var sleepQualitySum = 0;
        var hoursOfSleepSum = 0;
        var stressSum = 0;
        var heartrateSum = 0;
        let numAnswered = DataSnapshot.child("wellnessAnswered").val();
        if(numAnswered == null){
            return null;
        }
        DataSnapshot.child("players").forEach(function(child) {
            if(child.hasChild('WQ')){
                fatigueSum += child.child('WQ').child("fatigue").val();
                sorenessSum += child.child('WQ').child("soreness").val();
                moodSum += child.child('WQ').child("mood").val();
                sleepQualitySum += child.child('WQ').child("sleepQuality").val();
                stressSum += child.child('WQ').child("stress").val();
                hoursOfSleepSum += child.child('WQ').child("hoursOfSleep").val();
                heartrateSum += child.child('WQ').child("heartRate").val();
            }
        });
        
        let fatigueAvg = fatigueSum/numAnswered;
        let hoursOfSleepAvg = hoursOfSleepSum/numAnswered;
        let moodAvg = moodSum/numAnswered
        let sleepQualityAvg = sleepQualitySum/numAnswered;
        let sorenessAvg = sorenessSum/numAnswered;
        let stressAvg = stressSum/numAnswered;
        let heartrateAvg = heartrateSum/numAnswered;  
        let newAverages = {
            avgFatigue: fatigueAvg,
            avgHoursOfSleep: hoursOfSleepAvg,
            avgMood: moodAvg,
            avgSleepQuality: sleepQualityAvg,
            avgSoreness: sorenessAvg,
            avgStress: stressAvg,
            mentalReadiness: ((moodAvg + stressAvg + sleepQualityAvg)/15)*100,
            physicalReadiness: ((sorenessAvg + fatigueAvg)/10)*100,
            wellnessGrade: (fatigueAvg + moodAvg + sleepQualityAvg + sorenessAvg + stressAvg)*4,
            avgHeartRate: heartrateAvg
        };
        return event.data.ref.parent.child("averages").update(newAverages);
      });
    });

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

exports.findPlayerAverages = functions.database.ref('/players/{userID}/teams/{teamID}/wellnessAnswered').onWrite(event => {
      // Grab the current value of what was written to the Realtime Database.
      let snapshot = event.data.ref.parent;
      return snapshot.once('value').then(DataSnapshot => {
        if(DataSnapshot.val() == null){
            return null;
        }
        var fatigueSum = 0;
        var sorenessSum = 0;
        var moodSum = 0;
        var sleepQualitySum = 0;
        var hoursOfSleepSum = 0;
        var stressSum = 0;
        var heartrateSum = 0;
        
        let numAnswered = DataSnapshot.child("wellnessAnswered").val();
        if(numAnswered == null){
            return null;
        }
        DataSnapshot.child("dates").forEach(function(child) {
            if(child.hasChild('WQ')){
                fatigueSum += child.child('WQ').child("fatigue").val();
                sorenessSum += child.child('WQ').child("soreness").val();
                moodSum += child.child('WQ').child("mood").val();
                sleepQualitySum += child.child('WQ').child("sleepQuality").val();
                stressSum += child.child('WQ').child("stress").val();
                hoursOfSleepSum += child.child('WQ').child("hoursOfSleep").val();
                heartrateSum += child.child('WQ').child("heartRate").val();
            }
        });
        
        let fatigueAvg = fatigueSum/numAnswered;
        let hoursOfSleepAvg = hoursOfSleepSum/numAnswered;
        let moodAvg = moodSum/numAnswered
        let sleepQualityAvg = sleepQualitySum/numAnswered;
        let sorenessAvg = sorenessSum/numAnswered;
        let stressAvg = stressSum/numAnswered;
        let heartrateAvg = heartrateSum/numAnswered;  
        let newAverages = {
            avgFatigue: fatigueAvg,
            avgHoursOfSleep: hoursOfSleepAvg,
            avgMood: moodAvg,
            avgSleepQuality: sleepQualityAvg,
            avgSoreness: sorenessAvg,
            avgStress: stressAvg,
            mentalReadiness: ((moodAvg + stressAvg + sleepQualityAvg)/15)*100,
            physicalReadiness: ((sorenessAvg + fatigueAvg)/10)*100,
            wellnessGrade: (fatigueAvg + moodAvg + sleepQualityAvg + sorenessAvg + stressAvg)*4,
            avgHeartRate: heartrateAvg
        };
        return event.data.ref.parent.child("averages").update(newAverages);
      });
    });

exports.playerWellnessGrade = functions.database.ref('players/{userID}/teams/{teamID}/dates/{dateID}/WQ').onWrite(event =>{
    let snapshot = event.data.ref.parent;
    return snapshot.child('WQ').once('value').then(DataSnapshot => { 
        if(DataSnapshot.val() == null){
            return null;
        }
        let fatigue = DataSnapshot.child('fatigue').val();
        let mood = DataSnapshot.child('mood').val();
        let sleepQuality = DataSnapshot.child('sleepQuality').val();
        let soreness = DataSnapshot.child('soreness').val();
        let stress = DataSnapshot.child('stress').val();
        
        let wellnessGrade = (fatigue + mood + sleepQuality + soreness + stress)*4;
        return snapshot.child('wellnessGrade').set(wellnessGrade);
    });
  });

exports.playerMentalGrade = functions.database.ref('players/{userID}/teams/{teamID}/dates/{dateID}/WQ').onWrite(event =>{
    let snapshot = event.data.ref.parent;
    return snapshot.child('WQ').once('value').then(DataSnapshot => {
        if(DataSnapshot.val() == null){
            return null;
        }
        let mood = DataSnapshot.child('mood').val();
        let sleepQuality = DataSnapshot.child('sleepQuality').val();
        let stress = DataSnapshot.child('stress').val();
        
        let grade = ((mood + sleepQuality + stress)/15)*100;
        return snapshot.child('mentalReadiness').set(grade);
    });
  });

exports.playerPhysicalGrade = functions.database.ref('players/{userID}/teams/{teamID}/dates/{dateID}/WQ').onWrite(event =>{
    let snapshot = event.data.ref.parent;
    return snapshot.child('WQ').once('value').then(DataSnapshot => {
        if(DataSnapshot.val() == null){
            return null;
        }
        let fatigue = DataSnapshot.child('fatigue').val();
        let soreness = DataSnapshot.child('soreness').val();
        
        let grade = ((fatigue +soreness)*10);
        return snapshot.child('physicalReadiness').set(grade);
    });
  });


//should this be a cloud function??
exports.runningPlayerAverage = functions.database.ref('players/{userID}/teams/{teamID}/dates/{dateID}/WQ').onWrite(event =>{
    let snapshot = event.data.ref.parent.parent;
    let runningAverageSnapshot = event.data.ref.parent.parent.parent;
    
    let today = new Date();

    var dates = [];

    var todayString;

    var displayDate;
    if(today.getDate() <10){
        displayDate = "0"+(today.getDate()).toString();
    }
    else{
        displayDate = (today.getDate()).toString();
    }
    var displayMonth;
    if(today.getMonth() +1 <10){
        displayMonth = "0"+(today.getMonth()+1).toString();
    }
    else{
        displayMonth = (today.getMonth()+1).toString();
    }
    var displayYear = (today.getFullYear()).toString();
    var officialDisplayFullDate = displayMonth+"-"+displayDate+"-"+displayYear;

    dates.push(officialDisplayFullDate);
    var i;
    for (i = 1; i < 7; i++) { 
        var day = new Date(today);
        day.setDate(today.getDate() - i);

        if(day.getDate() <10){
            displayDate = "0"+(day.getDate()).toString();
        }
        else{
            displayDate = (day.getDate()).toString();
        }

        if(day.getMonth() +1 <10){
            displayMonth = "0"+(day.getMonth()+1).toString();
        }
        else{
            displayMonth = (day.getMonth()+1).toString();
        }
        displayYear = (day.getFullYear()).toString();
        officialDisplayFullDate = displayMonth+"-"+displayDate+"-"+displayYear;
        dates.push(officialDisplayFullDate);
    }

    //gonna need one for each attribute
    var fatigueDateMap =  new Map();
    var stressDateMap =  new Map();
    var sorenessDateMap =  new Map();
    var hoursOfSleepDateMap =  new Map();
    var moodDateMap =  new Map();
    var sleepQualityDateMap =  new Map();
    var gradeDateMap =  new Map();

    for (i = 0; i < 7; i++) { 
        fatigueDateMap.set(dates[i], null);
        stressDateMap.set(dates[i], null);
        sorenessDateMap.set(dates[i], null);
        hoursOfSleepDateMap.set(dates[i], null);
        moodDateMap.set(dates[i], null);
        sleepQualityDateMap.set(dates[i], null);
        gradeDateMap.set(dates[i], null);
    }
    
    return runningAverageSnapshot.once('value').then(DataSnapshot => {
        if(DataSnapshot.val() == null){
            return null;
        }
        
        fatigueDateMap.forEach(function(value, key, map) {
            if(DataSnapshot.child("dates").child(key).exists()){
                //they have answered on this day
                if(DataSnapshot.child("dates").child(key).child("WQ").exists()){
                    //they have answered a WQ on this day
                    fatigueDateMap.set(key, DataSnapshot.child("dates").child(key).child("WQ").child("fatigue").val());
                }
            }
        });
        
        stressDateMap.forEach(function(value, key, map) {
            
            if(DataSnapshot.child("dates").child(key).exists()){
                //they have answered on this day
                if(DataSnapshot.child("dates").child(key).child("WQ").exists()){
                    //they have answered a WQ on this day
                    stressDateMap.set(key, DataSnapshot.child("dates").child(key).child("WQ").child("stress").val());
                }
            }
        });
        
        sorenessDateMap.forEach(function(value, key, map) {
            
            if(DataSnapshot.child("dates").child(key).exists()){
                //they have answered on this day
                if(DataSnapshot.child("dates").child(key).child("WQ").exists()){
                    //they have answered a WQ on this day
                    sorenessDateMap.set(key, DataSnapshot.child("dates").child(key).child("WQ").child("soreness").val());
                }
            }
        });
        
        hoursOfSleepDateMap.forEach(function(value, key, map) {
            
            if(DataSnapshot.child("dates").child(key).exists()){
                //they have answered on this day
                if(DataSnapshot.child("dates").child(key).child("WQ").exists()){
                    //they have answered a WQ on this day
                    hoursOfSleepDateMap.set(key, DataSnapshot.child("dates").child(key).child("WQ").child("hoursOfSleep").val());
                }
            }
        });
        
        moodDateMap.forEach(function(value, key, map) {
            
            if(DataSnapshot.child("dates").child(key).exists()){
                //they have answered on this day
                if(DataSnapshot.child("dates").child(key).child("WQ").exists()){
                    //they have answered a WQ on this day
                    moodDateMap.set(key, DataSnapshot.child("dates").child(key).child("WQ").child("mood").val());
                }
            }
        });
        
        sleepQualityDateMap.forEach(function(value, key, map) {
            
            if(DataSnapshot.child("dates").child(key).exists()){
                //they have answered on this day
                if(DataSnapshot.child("dates").child(key).child("WQ").exists()){
                    //they have answered a WQ on this day
                    sleepQualityDateMap.set(key, DataSnapshot.child("dates").child(key).child("WQ").child("sleepQuality").val());
                }
            }
        });
        
        gradeDateMap.forEach(function(value, key, map) {
            
            if(DataSnapshot.child("dates").child(key).exists()){
                //they have answered on this day
                if(DataSnapshot.child("dates").child(key).child("WQ").exists()){
                    //they have answered a WQ on this day
                    gradeDateMap.set(key, DataSnapshot.child("dates").child(key).child("wellnessGrade").val());
                }
            }
        });
        
        //calculate the averages
        
        var count = 0;
        var sum = 0;
        
        var fatigueAvg = 0;
        var stressAvg = 0;
        var sorenessAvg = 0;
        var hoursAvg = 0;
        var moodAvg = 0;
        var qualityAvg = 0;
        var gradeAvg = 0;
        
        fatigueDateMap.forEach(function(value, key, map) {
            
            if(value != null){
                count++;
                sum += value
            }
            
        });
        
        if(count != 0){
            fatigueAvg = sum/count;
        }
        
        //reset values to calculate new average
        count = 0;
        sum = 0;
        
        stressDateMap.forEach(function(value, key, map) {
            
            if(value != null){
                count++;
                sum += value
            }
            
        });
        
        if(count != 0){
            stressAvg = sum/count;
        }
        
        //reset values to calculate new average
        count = 0;
        sum = 0;
        
        sorenessDateMap.forEach(function(value, key, map) {
            
            if(value != null){
                count++;
                sum += value
            }
            
        });
        
        if(count != 0){
            sorenessAvg = sum/count;
        }
        
        //reset values to calculate new average
        count = 0;
        sum = 0;
        
        hoursOfSleepDateMap.forEach(function(value, key, map) {
            
            if(value != null){
                count++;
                sum += value
            }
            
        });
        
        if(count != 0){
            hoursAvg = sum/count;
        }
        
        //reset values to calculate new average
        count = 0;
        sum = 0;
        
        moodDateMap.forEach(function(value, key, map) {
            
            if(value != null){
                count++;
                sum += value
            }
            
        });
        
        if(count != 0){
            moodAvg = sum/count;
        }
        
        //reset values to calculate new average
        count = 0;
        sum = 0;
        
        sleepQualityDateMap.forEach(function(value, key, map) {
            
            if(value != null){
                count++;
                sum += value
            }
            
        });
        
        if(count != 0){
            qualityAvg = sum/count;
        }
        
        //reset values to calculate new average
        count = 0;
        sum = 0;
        
        
        if(count != 0){
            gradeAvg = sum/count;
        }
        
        let runningAverage = {
            avgFatigue: fatigueAvg,
            avgHoursOfSleep: hoursAvg,
            avgMood: moodAvg,
            avgSleepQuality: qualityAvg,
            avgSoreness: sorenessAvg,
            avgStress: stressAvg,
        };
        
        
        return runningAverageSnapshot.child("runningAverage").update(runningAverage);
    });
  });


exports.addPlayerInfoToTeam = functions.database.ref('/players/{userID}/teams/{teamID}').onCreate(event => {
      // Grab the current value of what was written to the Realtime Database.
      let snapshot = event.data.ref.parent.parent.parent.parent;
      let user = event.params.userID;
      var email;
      var team;
      var newTeam;
      var newTeamInfo;
      return snapshot.once('value').then(DataSnapshot => {
        
        email = DataSnapshot.child('players').child(user).child('playerIdentifier').val();
        
        DataSnapshot.child("players").child(user).child("teams").forEach(function(child) {
            team = child.key;
            
            //maneuver to the team branch
            if(!DataSnapshot.child("teams").child(team).child("players").child(user).exists()){
                //this is the first time the player has joined so we must create the streak
                newTeam = team;
                newTeamInfo = {
                    playerIdentifier: email,
                    haveTaken: false,
                    streak : 0
                };
            }
            else{
                //give it the info it already has
            }

        });
          
        if(newTeam != null){
             return event.data.ref.parent.parent.parent.parent.child("teams").child(newTeam).child("players").child(user).set(newTeamInfo);
        }else{
            //team branches are already updated
             return event.data.ref.parent.parent.parent.parent.child("teams").child(team).child("players").child(user).update({playerIdentifier: email});
        }
           
      });
    });

//cloud function to increment numplayers in teams
exports.incrementNumPlayers = functions.database.ref('/teams/{teamID}/players').onWrite(event => {
    let snapshot = event.data.ref.parent;
    //inspiration from https://stackoverflow.com/questions/42914815/cloud-functions-for-firebase-increment-counter
    let countRef = snapshot.child('numPlayers');
    return snapshot.once('value').then((messagesData) => {
        //need to check if players branch even exists
        if(messagesData.val() == null){
            return null;
        }
        if(messagesData.child("players").exists()){
            return countRef.set(messagesData.child("players").numChildren());
        }
        else{
            return countRef.set(0);
        }
    });
});

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//cloud function for timed push notifications  7-8 am, 11-12 am, 7-8 pm, 10-11 pm

exports.checkToSendWellness = functions.https.onRequest((req, res) => {
    const databaseSnapshot = admin.database().ref();
    var tempdate = new Date();
    
    var estOffset = -300;
    const date = new Date( tempdate.getTime() + estOffset*60*1000 );
    const weekday = date.getDay(); //sunday is 0 and saturday is 6
    var displayDate;
    if(date.getDate() <10){
        displayDate = "0"+(date.getDate()).toString();
    }
    else{
        displayDate = (date.getDate()).toString();
    }
    var displayMonth;
    if(date.getMonth() +1 <10){
        displayMonth = "0"+(date.getMonth()+1).toString();
    }
    else{
        displayMonth = (date.getMonth()+1).toString();
    }
    const displayYear = (date.getFullYear()).toString();
    const officialDisplayFullDate = displayMonth+"-"+displayDate+"-"+displayYear;
    
    var promises = [];
    
    const databaseValues = databaseSnapshot.once('value').then(DataSnapshot => {
        DataSnapshot.child("teams").forEach(function(child) {
            team = child.key;
            
            //value that holds whether a notification is to be sent or not
            var notification;
            
            //check if that team is scheduled to send push notifications
            //if so change wellnessTrigger value
            if(weekday == 0){
                //sunday
                notification = child.child("notifications").child("WQ").child("sunday").val();
            }else if(weekday == 1){
                //monday
                notification = child.child("notifications").child("WQ").child("monday").val();
            }else if(weekday == 2){
                //tuesday
                notification = child.child("notifications").child("WQ").child("tuesday").val();
            }else if(weekday == 3){
                //wednesday
                notification = child.child("notifications").child("WQ").child("wednesday").val();
            }else if(weekday == 4){
                //thursday
                notification = child.child("notifications").child("WQ").child("thursday").val();
            }else if(weekday == 5){
                //friday
                notification = child.child("notifications").child("WQ").child("friday").val();
            }else if(weekday == 6){
                //saturday
                notification = child.child("notifications").child("WQ").child("saturday").val();
            }
            
            
            if(notification){
                promises.push(sendWellness(res,team, officialDisplayFullDate));
            }
            
        });
    });
    //const urlPromise = res.send('Wellness Sent Successful');
    //promises.push(urlPromise);
    promises.push(databaseValues); 
    
    Promise.all(promises).then(function(){
        return res.send('Wellness Sent Successfully');
    }).catch(function(err){
        console.log(err);
        return res.send('Wellness Sent Unsuccessfully');;
    });
});

function sendWellness(res, team, date){
    const databaseSnapshot = admin.database().ref();
    const databasePlayersSnapshot = admin.database().ref().child("teams").child(team).child("players");
    var promises = [];
    const databaseValues = databasePlayersSnapshot.once('value').then(DataSnapshot => {
        DataSnapshot.forEach(function(child) {
            let playerID = child.key;
            promises.push(checkStreak(res, "WQ", team, playerID,date));
            
        });
    });
    
    promises.push(databaseValues);
    
    return Promise.all(promises);
    
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//rpe
exports.checkToSendRPE = functions.https.onRequest((req, res) => {
    const databaseSnapshot = admin.database().ref();
    var tempdate = new Date();
    
    var estOffset = -300;
    const date = new Date( tempdate.getTime() + estOffset*60*1000 );
    const weekday = date.getDay(); //sunday is 0 and saturday is 6
    var displayDate;
    if(date.getDate() <10){
        displayDate = "0"+(date.getDate()).toString();
    }
    else{
        displayDate = (date.getDate()).toString();
    }
    var displayMonth;
    if(date.getMonth() +1 <10){
        displayMonth = "0"+(date.getMonth()+1).toString();
    }
    else{
        displayMonth = (date.getMonth()+1).toString();
    }
    const displayYear = (date.getFullYear()).toString();
    const officialDisplayFullDate = displayMonth+"-"+displayDate+"-"+displayYear;
    
    var promises = [];
    
    const databaseValues = databaseSnapshot.once('value').then(DataSnapshot => {
        DataSnapshot.child("teams").forEach(function(child) {
            team = child.key;
            
            //value that holds whether a notification is to be sent or not
            var notification;
            
            //check if that team is scheduled to send push notifications
            //if so change wellnessTrigger value
            if(weekday == 0){
                //sunday
                notification = child.child("notifications").child("RPE").child("sunday").val();
            }else if(weekday == 1){
                //monday
                notification = child.child("notifications").child("RPE").child("monday").val();
            }else if(weekday == 2){
                //tuesday
                notification = child.child("notifications").child("RPE").child("tuesday").val();
            }else if(weekday == 3){
                //wednesday
                notification = child.child("notifications").child("RPE").child("wednesday").val();
            }else if(weekday == 4){
                //thursday
                notification = child.child("notifications").child("RPE").child("thursday").val();
            }else if(weekday == 5){
                //friday
                notification = child.child("notifications").child("RPE").child("friday").val();
            }else if(weekday == 6){
                //saturday
                notification = child.child("notifications").child("RPE").child("saturday").val();
            }
            
            
            if(notification){
                promises.push(sendRPE(res,team, officialDisplayFullDate));
            }
            
        });
    });
    //const urlPromise = res.send('RPE Sent Successful');
    //promises.push(urlPromise);
    promises.push(databaseValues);       
    Promise.all(promises).then(function(){
        return res.send('RPE Sent Successfully');
    }).catch(function(err){
        console.log(err);
        return res.send('RPE Sent Unsuccessfully');;
    });
});

function sendRPE(res, team, date){
    const databaseSnapshot = admin.database().ref();
    const databasePlayersSnapshot = admin.database().ref().child("teams").child(team).child("players");
    var promises = [];
    const databaseValues = databasePlayersSnapshot.once('value').then(DataSnapshot => {
        DataSnapshot.forEach(function(child) {
            let playerID = child.key;
            //check to see if we need to reset streak
            promises.push(checkStreak(res, "RPE", team, playerID,date));
            
        });
    });
    
    promises.push(databaseValues);
    
    return Promise.all(promises);
    
}

function checkStreak(res, questValue, team, player,date){
    const databaseSnapshot = admin.database().ref();
    const questionnaireSnapshot = databaseSnapshot.child("players").child(player).child("teams").child(team).child("questionnaires");
    const streakSnapshot = databaseSnapshot.child("teams").child(team).child("players").child(player).child("streak");
    var promises = [];
    const databaseValues = questionnaireSnapshot.once('value').then(DataSnapshot => {
        if(DataSnapshot.child(questValue).exists()){
            //they have a questionnaire so we need to reset thier streak
            
            promises.push(streakSnapshot.set(0));
        }
        promises.push(placeQuest(res, questValue, team, player, date));
    });
    promises.push(databaseValues);
    return Promise.all(promises);
}

function placeQuest(res, questValue, team, player,date){
    const databaseSnapshot = admin.database().ref();
    const questionnaireSnapshot = databaseSnapshot.child("players").child(player).child("teams").child(team).child("questionnaires");
    const streakSnapshot = databaseSnapshot.child("teams").child(team).child("players").child(player).child("streak");
    var promises = [];
    
    const databaseValues = questionnaireSnapshot.once('value').then(DataSnapshot => {
        //insert the questionnaire
        
        if(questValue == "RPE"){
            promises.push(questionnaireSnapshot.child(questValue).set(date + " 19:00:00"));
            console.log("RPE inserted for " + player+ " on team " + team);
            
        }
        else if(questValue == "WQ"){
            promises.push(questionnaireSnapshot.child(questValue).set(date + " 08:00:00"));
            console.log("Wellness inserted for " + player+ " on team " + team);
            
        }
        
        
        
    });
    
    promises.push(databaseValues);
    return Promise.all(promises);
}

//if a questionnaire was added, send notification
//we only want this function to execute if new data was added
exports.sendNotifictions = functions.database.ref('/players/{playerID}/teams/{teamID}/questionnaires/{questionnaireID}').onCreate(event => {
    
    let snapshot = event.data.ref.parent;
    let teamSnapshot = event.data.ref.parent.parent;
    let playerSnapshot = event.data.ref.parent.parent.parent.parent;
    
    let teamNamePromise = teamSnapshot.child('teamName').once('value');
    let tokenPromise = playerSnapshot.child('instanceId').once('value');
    return Promise.all([teamNamePromise,tokenPromise]).then((results) => {        
        //check to see if they have token
        if(!results[1].exists()){
            
            return null;
        }
        
        
                                                                    
        const team = results[0].val();
        const token = results[1].val();
        
        const payload = {
            notification:{
                title: 'Sport Report Questionnaire!',
                body: 'Take your new questionnaire for '+team+'.',
                sound: "default",
                badge: "1"
            },
            
        };
        
        return admin.messaging().sendToDevice(token, payload);
        
    }).then((response) => {
        // For each message check if there was an error.
        const tokensToRemove = [];
    
        if(response == null){
            return null;
        }
        response.results.forEach((result, index) => {
            const error = result.error;
            if (error) {
                console.error('Failure sending notification to', tokens[index], error);
                // Cleanup the tokens who are not registered anymore.
                if (error.code === 'messaging/invalid-registration-token' || error.code === 'messaging/registration-token-not-registered') {
                    //tokensToRemove.push(tokensSnapshot.ref.child(tokens[index]).remove());
                    
                }
            }
            else{
                console.log(event.params.playerID + " should recieve a notification.");
            }
            
        });
        return Promise.all(tokensToRemove);
    });
    
    
    
});


//cloud function to increment wellnessAnswered and rpeAnswered
exports.incrementPlayerAnswered = functions.database.ref('players/{userID}/teams/{teamID}/dates/{dateID}').onWrite(event => {
    let snapshot = event.data.ref.parent.parent;
    
    return snapshot.once('value').then(DataSnapshot => {
        if(DataSnapshot.val() == null){
            return null;
        }
        var wellnessSum = 0;
        var rpeSum = 0;
        DataSnapshot.child("dates").forEach(function(child) {
            if(child.hasChild('WQ')){
                wellnessSum += 1;
            }
            if(child.hasChild('RPE')){
                rpeSum += 1;
            }
        });
         
        let updates = {
            wellnessAnswered: wellnessSum,
            rpeAnswered: rpeSum
        };
        return snapshot.update(updates);
      });
});

exports.incrementTeamAnswered = functions.database.ref('teams/{teamID}/wellnessData/{dateID}/players/{playerID}').onWrite(event => {
    let snapshot = event.data.ref.parent.parent;
    
    return snapshot.once('value').then(DataSnapshot => {
        if(DataSnapshot.val() == null){
            return null;
        }
        var wellnessSum = 0;
        var rpeSum = 0;
        DataSnapshot.child("players").forEach(function(child) {
            if(child.hasChild('WQ')){
                wellnessSum += 1;
            }
            if(child.hasChild('RPE')){
                rpeSum += 1;
            }
        });
         
        let updates = {
            wellnessAnswered: wellnessSum,
            rpeAnswered: rpeSum
        };
        return snapshot.update(updates);
      });
});



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//RPE

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//once and rpe is added to the team branch recalculate average rpe
exports.averageTeamRPE = functions.database.ref('teams/{teamID}/wellnessData/{dateID}/rpeAnswered').onWrite(event => {
    let snapshot = event.data.ref.parent;
    
    return snapshot.once('value').then(DataSnapshot => {
        if(DataSnapshot.val() == null){
            return null;
        }
        var rpeSum = 0;
        var numAnswered = DataSnapshot.child("rpeAnswered").val();
        if(numAnswered == null){
            return null;
        }
        
        DataSnapshot.child("players").forEach(function(child) {
            if(child.hasChild('RPE')){
                rpeSum += parseFloat(child.child('RPE').child('rpe').val());
               
            }
        });
        
        var avg;
        if(numAnswered != 0){
            avg = rpeSum/numAnswered;
        }
        else{
            avg = null;
        }
        
       
         
        let updates = {
            avgRPE: avg
        };
        return snapshot.child("averages").update(updates);
        
      });
});

//once time is added to a specific day calculate workload. 
exports.calculateWorkload = functions.database.ref('teams/{teamID}/wellnessData/{dateID}/timePlayed').onWrite(event => {
    let snapshot = event.data.ref.parent;
    
    return snapshot.once('value').then(DataSnapshot => {
        if(DataSnapshot.val() == null){
            return null;
        }
        var rpe = DataSnapshot.child("averages").child("avgRPE").val();
        var time = DataSnapshot.child("timePlayed").val();
        
        var workload;
        if(rpe != null && time != null){
            workload = rpe*time;
        }
        else{
            workload = null;
        }
        
        
         
        let updates = {
            workload: workload
        };
        return snapshot.update(updates);
        
      });
});

exports.updateWorkload = functions.database.ref('teams/{teamID}/wellnessData/{dateID}/averages/avgRPE').onWrite(event => {
    let snapshot = event.data.ref.parent.parent;
    
    return snapshot.once('value').then(DataSnapshot => {
        if(DataSnapshot.val() == null){
            return null;
        }
        //don't do anything if the coach has not entered timePlayed
        if(DataSnapshot.child("timePlayed").exists()){
            var rpe = DataSnapshot.child("averages").child("avgRPE").val();
            var time = DataSnapshot.child("timePlayed").val();
            
            var workload;
            if(rpe != null && time != null){
                workload = rpe*time;
            }
            else{
                workload = null;
            }

            

            let updates = {
                workload: workload
            };
            return snapshot.update(updates);
        }
        else{
            return null;
        }
        
      });
});

//time cloud function changes boolean within every team branch if it
exports.ReminderTrigger = functions.https.onRequest((req, res) => {
    const databaseSnapshot = admin.database().ref();
    var promises = [];
    
    const databaseValues = databaseSnapshot.once('value').then(DataSnapshot => {
        DataSnapshot.child("players").forEach(function(player) {
            const playerID = player.key;
            promises.push(checkEachPlayersTeam(res, playerID));
        });
    });
    
    //const urlPromise = res.send('Reminder Sent Successful');
    //promises.push(urlPromise);
    promises.push(databaseValues);
    
    Promise.all(promises).then(function(){
        return res.send('Reminder Sent Successfully');
    }).catch(function(err){
        console.log(err);
        return res.send('Reminder Sent Unsuccessfully');;
    });
});

function checkEachPlayersTeam(res, playerID){
    const databaseSnapshot = admin.database().ref().child("players").child(playerID).child("teams");
    var promises = [];
    
    const databaseValues = databaseSnapshot.once('value').then(DataSnapshot => {
        DataSnapshot.forEach(function(team) {
            const teamID = team.key;
            if(team.child("questionnaires").exists()){
                //they have at least one questionaire so a reminder should be sent
                promises.push(sendReminder(res, playerID, teamID));
            }
        });
    });
    
    promises.push(databaseValues);       
    return Promise.all(promises);
}

function sendReminder(res, playerID, teamID){
    const databaseSnapshot = admin.database().ref().child("players").child(playerID);
    let tokenPromise = databaseSnapshot.child('instanceId').once('value');
    let teamNamePromise = databaseSnapshot.child('teams').child(teamID).child("teamName").once('value');
    return Promise.all([teamNamePromise,tokenPromise]).then((results) => {        
        //check to see if they have token
        if(!results[1].exists()){
            console.log("User " + playerID+ " has no token.");
            return null;
        }
                                                                    
        const team = results[0].val();
        const token = results[1].val();
        const disply = "Don't lose your streak! Answer your questionnaire for "+team+".";
        
        const payload = {
            notification:{
                title: 'Sport Report Questionnaire!',
                body: "Don't lose your streak! Answer your questionnaire for "+team+".",
                sound: "default",
                badge: "1"
            }
        }
        
        
        return admin.messaging().sendToDevice(token, payload);
        
    }).then((response) => {
        // For each message check if there was an error.
        const tokensToRemove = [];
        
        if(response == null){
            return null;
        }
        response.results.forEach((result, index) => {
            const error = result.error;
            if (error) {
                console.error('Failure sending notification to', tokens[index], error);
                // Cleanup the tokens who are not registered anymore.
                if (error.code === 'messaging/invalid-registration-token' || error.code === 'messaging/registration-token-not-registered') {
                    //tokensToRemove.push(tokensSnapshot.ref.child(tokens[index]).remove());
                    console.log('need to remove tokens')
                }
            }
        });
        return Promise.all(tokensToRemove);
    });
}

//cron job to set all haveTaken values to false
exports.StreakMaintenanceReset = functions.https.onRequest((req, res) => {
    const databaseSnapshot = admin.database().ref().child("teams");
    var promises = [];
    
    const databaseValues = databaseSnapshot.once('value').then(DataSnapshot => {
        DataSnapshot.forEach(function(team) {
            const teamID = team.key;
            promises.push(setToFalse(res, teamID));
        });
    });
    
    //const urlPromise = res.send('Reset Sent Successful');
    //promises.push(urlPromise);
    promises.push(databaseValues);       
    Promise.all(promises).then(function(){
        return res.send('Reset Sent Successfully');
    }).catch(function(err){
        console.log(err);
        return res.send('Reset Sent Unsuccessfully');;
    });
});

function setToFalse(res,teamID){
    const databaseSnapshot = admin.database().ref().child("teams").child(teamID).child("players");
    var promises = [];
    
    const databaseValues = databaseSnapshot.once('value').then(DataSnapshot => {
        DataSnapshot.forEach(function(player) {
            const playerID = player.key;
            promises.push(databaseSnapshot.child(playerID).child("haveTaken").set(false));
        });
    });
    
    promises.push(databaseValues);       
    return Promise.all(promises);
}

//cloud function, once a questionaire is taken(deleted), change haveTaken value and increment the streak
exports.incrementStreak = functions.database.ref('players/{userID}/teams/{teamID}/questionnaires/{questID}').onDelete(event => {    
    let playerID = event.params.userID;
    let teamID = event.params.teamID;
    let questID = event. params.questID;
    
    let snapshot = event.data.ref.parent.parent.parent.parent.parent.parent.child("teams").child(teamID).child("players").child(playerID);
    
    return snapshot.once('value').then(DataSnapshot => {
        if(DataSnapshot.child("haveTaken").val() == false){
            //set to true and increment
            let streak = DataSnapshot.child("streak").val();

            let updates = {
                streak: streak+1,
                haveTaken: true
            };
            return snapshot.update(updates);
        }
        else{
            //do nothing
            return null;
        }
        
        
      });
});