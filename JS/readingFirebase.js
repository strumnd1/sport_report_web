var uid;
var team;
var date;
var dbRef;

$(document).ready( function() {

    checkIfLoggedIn()
  .then(function() {
    return getTeamCode();
  })
  .then(function(teamCode) {
    team = teamCode;
    displayTeamAverageData(makeTeamReference(uid, team));
  })
  .catch(function(err){
    console.log(err);
    //if(err === "No team code exists for this coach"){}
  });


});

function checkIfLoggedIn(){
  // Setup the log out button
  document.getElementById("tempLogoutButton").addEventListener("click", function(){
    firebase.auth().signOut()
  });
  return new Promise(function (resolve, reject) {
    //establish if the user is there
    firebase.auth().onAuthStateChanged(firebaseUser => {
      // Check if the user is logged in or not
      if(firebaseUser){
        uid = firebase.auth().currentUser.uid;
        // Also save what our email is at this part.
        coachesEmail = firebase.auth().currentUser.email;
        return resolve();
      }else{
        // The user logged out
        // Clear sessionStorage and redirect them to login screen
        sessionStorage.clear();
        location.replace("SignUp/index.html");
        return reject("User is not logged in");
      }
    });
  });
}

function getTeamCode(){
  return new Promise(function (resolve, reject) {
    try {
      if(typeof(Storage) !== "undefined") {
        if(sessionStorage.teamCode){
          return resolve(sessionStorage.teamCode);
        }else{
          throw "no teamcode";
        }
      }else {
        console.log("Sorry, your browser does not support web storage...");
        throw "no teamcode";
      }
    }catch(e){
      // Our session didn't have the teamCode
      // Will have to manually retrieve the teamcode from firebase
      var uid = firebase.auth().currentUser.uid;
      const retrieveTeamCode = firebase.database().ref("coaches").child(firebase.auth().currentUser.uid).child("team");
      retrieveTeamCode.once('value').then(function(snapshot){
        var teamCode = snapshot.val();
        if(teamCode !== null){
          sessionStorage.teamCode = teamCode;
          return resolve(teamCode);
        }
        return reject("No team code exists for this coach");
      });
    }
  });
}

function calculateColorForPercentage(percentage){
  if(percentage >= 80){
    return "darkgreen";
  }else if(percentage >= 60 && percentage < 80){
    return "yellowgreen";
  }else if (percentage < 60 && percentage >= 40){
    return "yellow";
  }else if (percentage < 40 && percentage >= 20){
    return "orange";
  } else if (percentage < 20 && percentage >= 0){
    return "red";
  }else if (percentage == null){
    return "gray";
  }
}

// calculate percentage for each value
function calculatePercentage(value){
  if (value != null){
    return Math.floor((value/5)*100);
  } else {
    return "N/A"
  }
}

// calculate percentage for each value
function calculateMentalWellnessSuperScorePercentage(v1, v2, v3){
  if (v1 != null && v2 != null && v3 != null){
    return Math.floor(((v1 + v2 + v3)/15)*100);
  } else {
    return "N/A"
  }
}

// calculate percentage for each value
function calculatePhysicalWellnessSuperScorePercentage(v1, v2){
  if (v1 != null && v2 != null){
    return Math.floor(((v1 + v2)/10)*100);
  } else {
    return "N/A"
  }
}

// calculate percentage for each value
function calculateWellnessGradePercentage(v1, v2, v3, v4){
  if (v1 != null && v2 != null && v3 != null && v4 != null){
    return Math.floor((v1 + v2 + v3 + v4) * 4);
  } else {
    return "N/A"
  }
}


// add or do not add percent symbol
function percentSymbol(value){
  if (value == "N/A"){
    return ""
  }
  return "%"
}

function displayTeamAverageData(dbRef){
    //need to fetch the date and the team code of the coach upon logging in
    //const dbRef = firebase.database().ref("teams/nzOxeD/wellnessData/01-02-2001/averages");
    //read team average data
    //comment
    //inspired by: https://stackoverflow.com/questions/41427859/get-array-of-items-from-firebase-snapshot
    dbRef.on('value', function(snapshot){
        avgFatigue = snapshot.child("avgFatigue").val();
        console.log(avgFatigue);
        avgMood = snapshot.child("avgMood").val();
        console.log(avgMood);
        avgSleepQuality = snapshot.child("avgSleepQuality").val();
        console.log(avgSleepQuality);
        avgSoreness = snapshot.child("avgSoreness").val();
        console.log(avgSoreness);
        avgStress = snapshot.child("avgStress").val();
        console.log(avgStress);

        let mentalWellnessSuperScore = calculateMentalWellnessSuperScorePercentage(avgMood, avgSleepQuality, avgStress);
        console.log(mentalWellnessSuperScore);
        let physicalWellnessSuperScore = calculatePhysicalWellnessSuperScorePercentage(avgFatigue,avgSoreness);
        console.log(physicalWellnessSuperScore);
        let WellnessGrade = calculateWellnessGradePercentage(avgFatigue, avgMood, avgSleepQuality, avgSoreness+avgStress);
        console.log(WellnessGrade);
        let fatiguePercentage = calculatePercentage(avgFatigue)
        let moodPercentage = calculatePercentage(avgMood)
        let sleepQualityPercentage = calculatePercentage(avgStress)
        let sorenessPercentage = calculatePercentage(avgSoreness)
        let stressPercentage = calculatePercentage(avgStress)

        //establish colors for the circles
        var wellnessGradeColor;
        var physicalReadinessColor;
        var mentalReadinessColor;
        var fatigueColor;
        var moodColor;
        var sleepQualityColor;
        var sorenessColor;
        var stressColor;


        // calculate colors for wellness circles
        wellnessGradeColor = calculateColorForPercentage(WellnessGrade);
        console.log(wellnessGradeColor);
        mentalReadinessColor = calculateColorForPercentage(mentalWellnessSuperScore);
        console.log(mentalReadinessColor);
        physicalReadinessColor = calculateColorForPercentage(physicalWellnessSuperScore);
        console.log(physicalReadinessColor);
        fatigueColor = calculateColorForPercentage(fatiguePercentage);
        console.log(fatigueColor);
        moodColor = calculateColorForPercentage(moodPercentage);
        console.log(moodColor);
        sleepQualityColor = calculateColorForPercentage(sleepQualityPercentage);
        console.log(sleepQualityColor);
        sorenessColor = calculateColorForPercentage(sorenessPercentage);
        console.log(sorenessColor);
        stressColor = calculateColorForPercentage(stressPercentage);
        console.log(stressColor);

        //make function for freshly .once on avg. fatigue and stuff

         //wellness
        $("#wellnessGradeCircle").removeClass($("#wellnessGradeCircle").attr("class")).addClass("c100 p"+WellnessGrade+" big " + wellnessGradeColor);
        $("#wellnessGradeCircleText").html(WellnessGrade+percentSymbol(WellnessGrade));

        //emotional readiness\
        $("#emotionalReadinessCircle").removeClass($("#emotionalReadinessCircle").attr("class")).addClass("c100 p"+mentalWellnessSuperScore+" " + mentalReadinessColor);
        $("#emotionalReadinessCircleText").html(mentalWellnessSuperScore+percentSymbol(mentalWellnessSuperScore));

        //physical readiness
        $("#physicalReadinessCircle").removeClass($("#physicalReadinessCircle").attr("class")).addClass("c100 p"+physicalWellnessSuperScore+" " + physicalReadinessColor);
        $("#physicalReadinessCircleText").html(physicalWellnessSuperScore+percentSymbol(physicalWellnessSuperScore));

        //fatigue
        $("#fatigueCircle").removeClass($("#fatigueCircle").attr("class")).addClass("c100 p"+fatiguePercentage+" small " + fatigueColor);
        $("#fatigueCircleText").html(fatiguePercentage+percentSymbol(fatiguePercentage));

        //soreness
        $("#sorenessCircle").removeClass($("#sorenessCircle").attr("class")).addClass("c100 p"+sorenessPercentage+" small " + sorenessColor);
        $("#sorenessCircleText").html(sorenessPercentage+percentSymbol(sorenessPercentage));

        //mood
        $("#moodCircle").removeClass($("#moodCircle").attr("class")).addClass("c100 p"+moodPercentage+" small " + moodColor);
        $("#moodCircleText").html(moodPercentage+percentSymbol(moodPercentage));

        //sleep quality
        $("#sleepCircle").removeClass($("#sleepCircle").attr("class")).addClass("c100 p"+sleepQualityPercentage+" small " + sleepQualityColor);
        $("#sleepCircleText").html(sleepQualityPercentage+percentSymbol(sleepQualityPercentage));

        //stress
        $("#stressCircle").removeClass($("#stressCircle").attr("class")).addClass("c100 p"+stressPercentage+" small " + stressColor);
        $("#stressCircleText").html(stressPercentage+percentSymbol(stressPercentage));
    });
}

function makeTeamReference(uid, teamCode){
    if(teamCode){
        let tempDate = document.getElementById("datepickerElm").value;
        return firebase.database().ref("teams").child(teamCode).child("wellnessData").child(tempDate).child("averages");
        console.log("here");
    }
}
