/*
 *
 * Created by Samuel Kenney
 *
 * Copyright © 2018 Le Crew Studios. All Rights Reserved.
 *
*/

var uid;
var code;
var teamName;
var players
var player;
var playerInformation
var selectedValue = []; // keeps track of last selected value for each card
var singletonChart;
var coachesName;

$(document).ready( function() {
  // check if the user is logged in
  checkIfLoggedIn()
    .then(function() {
      // if they are logged in, get the code
      return getTeamCode();
    })
    .then(function(teamCode) {
      // Insert coachName into HTML
      if(coachesName){
        $('#coachName').html("Coach " + coachesName);
      }

      // retreive team information with new code
      const retreiveTeamInformation = firebase.database().ref("teams").child(teamCode).child("players");
      retreiveTeamInformation.once('value').then(function(snapshot){
        // store all players
        players = snapshot.val();
        // start a counter to set up unique elements
        var count = 1;
        // go through each player in the team
        for (var playerUID in players) {
          // makes sure not to add a player if nothing valid returns
          if (playerUID != null){
            const retreivePlayerInformation = firebase.database().ref("players").child(playerUID);
            // Set up database references for graph that will be used later
            var pastDateSeven = retreivePastDateBasedOnNumberofDays(7);
            var pastDateThirty = retreivePastDateBasedOnNumberofDays(30);
            var pastDateSixty = retreivePastDateBasedOnNumberofDays(60);
            var pastDateNinety = retreivePastDateBasedOnNumberofDays(90);

            const dateSortedSeven = firebase.database().ref("players/" + playerUID + "/teams/" + teamCode + "/dates").orderByKey().startAt(pastDateSeven);
            const teamDateSortedSeven = firebase.database().ref("teams/" + teamCode + "/wellnessData").orderByKey().startAt(pastDateSeven);
            const dateSortedThirty = firebase.database().ref("players/" + playerUID + "/teams/" + teamCode + "/dates").orderByKey().startAt(pastDateThirty);
            const teamDateSortedThirty = firebase.database().ref("teams/" + teamCode + "/wellnessData").orderByKey().startAt(pastDateThirty);
            const dateSortedSixty = firebase.database().ref("players/" + playerUID + "/teams/" + teamCode + "/dates").orderByKey().startAt(pastDateSixty);
            const teamDateSortedSixty = firebase.database().ref("teams/" + teamCode + "/wellnessData").orderByKey().startAt(pastDateSixty);
            const dateSortedNinety = firebase.database().ref("players/" + playerUID + "/teams/" + teamCode + "/dates").orderByKey().startAt(pastDateNinety);
            const teamDateSortedNinety = firebase.database().ref("teams/" + teamCode + "/wellnessData").orderByKey().startAt(pastDateNinety);


            retreivePlayerInformation.once('value').then(function(snapshot){
              playerInformation = snapshot.val();
              // grab identifier
              var playerIdentifier = snapshot.child("playerIdentifier").val();
              // if the player does not have an identifier, remove do no add the player
              if (playerIdentifier != null){

                var teams = snapshot.child("teams").val();

                // retreive current Date
                var currentDate = formatDate(new Date()).toString();
                // set up variables to be used in the html
                fatigue = snapshot.child("teams").child(teamCode).child("dates").child(currentDate).child("WQ").child("fatigue").val();
                soreness = snapshot.child("teams").child(teamCode).child("dates").child(currentDate).child("WQ").child("soreness").val();
                mood = snapshot.child("teams").child(teamCode).child("dates").child(currentDate).child("WQ").child("mood").val();
                sleep = snapshot.child("teams").child(teamCode).child("dates").child(currentDate).child("WQ").child("sleepQuality").val();
                stress = snapshot.child("teams").child(teamCode).child("dates").child(currentDate).child("WQ").child("stress").val();

                // calculate percentages for the different values
                let fatiguePercentage = calculatePercentage(fatigue);
                let sorenessPercentage = calculatePercentage(soreness);
                let moodPercentage = calculatePercentage(mood);
                let sleepQualityPercentage = calculatePercentage(sleep);
                let stressPercentage = calculatePercentage(stress);

                // set up colors for percentage circles on card
                fatigueColor = calculateColorForValues(fatigue);
                sorenessColor = calculateColorForValues(soreness);
                moodColor = calculateColorForValues(mood);
                sleepQualityColor = calculateColorForValues(sleep);
                stressColor = calculateColorForValues(stress);

                // retreive wellness, physical, and mental averages, already in percentage form
                let wellnessGrade = calculatePercentageForSuperScores(snapshot.child("teams").child(teamCode).child("dates").child(currentDate).child("wellnessGrade").val());
                let physicalReadiness = calculatePercentageForSuperScores(snapshot.child("teams").child(teamCode).child("dates").child(currentDate).child("physicalReadiness").val());
                let mentalReadiness = calculatePercentageForSuperScores(snapshot.child("teams").child(teamCode).child("dates").child(currentDate).child("mentalReadiness").val());

                // calculate color for wellness, physical, and mental percentage circles
                wellnessColor = calculateColorForPercentage(wellnessGrade);
                physicalReadinessColor = calculateColorForPercentage(physicalReadiness);
                mentalReadinessColor = calculateColorForPercentage(mentalReadiness);

                // keep track of and make unique each heading and card
                var cardCollapse = "collapse" + count;
                var cardHeading = "heading" + count;
                var canvasID = "canvas" + count;
                var buttonID = "button" + count;

                // set up HTML for each card for each player
                var card = "<!--player 1--> <div class=\"card \"><div class=\"card-header\" id=" + cardHeading + "><!-- Player Name button--><div class = \"row\"><div class=\"col-lg-2 \"><button id="+ buttonID + " class=\"btn btn-link\" data-toggle=\"collapse\" data-target=#"+ cardCollapse + " aria-controls="+ cardCollapse + ">"+playerIdentifier+"</button></div><!--Fatigue Percetnage Circle--><div class=\"col-lg-2\"><div id=\"playerCircle\" class=\"c100 p"+ fatiguePercentage +" smaller "+fatigueColor+"\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Fatigue\"><span>"+fatiguePercentage + percentSymbol(fatiguePercentage) + "</span> <div class=\"slice\"><div class= \"bar\"></div><div class=\"fill\"></div></div></div></div><!--Soreness Percentage Circle--><div class=\"col-lg-2\"><div id=\"playerCircle\" class=\"c100 p"+ sorenessPercentage +" smaller "+sorenessColor+"\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Soreness\"><span>"+sorenessPercentage + percentSymbol(sorenessPercentage) + "</span><div class=\"slice\"><div class= \"bar\"></div><div class=\"fill\" ></div></div></div></div><!--Mood Percentage Circle--><div class=\"col-lg-2\"><div id=\"playerCircle\" class=\"c100 p"+ moodPercentage +" smaller "+moodColor+"\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Mood\"><span>"+moodPercentage + percentSymbol(moodPercentage) + "</span><div class=\"slice\"><div class= \"bar\"></div><div class=\"fill\"></div></div></div></div><!--Stress Percentage Circle--><div class=\"col-lg-2\"><div id=\"playerCircle\" class=\"c100 p"+ stressPercentage +" smaller "+stressColor+"\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Stress\"><span>"+stressPercentage + percentSymbol(stressPercentage) + "</span><div class=\"slice\"><div class= \"bar\"></div><div class=\"fill\"></div></div></div></div><!--Sleep Percentage Circle--><div class=\"col-lg-2\"><div id=\"playerCircle\" class=\"c100 p"+ sleepQualityPercentage +" smaller "+sleepQualityColor+"\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Sleep\"><span>"+sleepQualityPercentage + percentSymbol(sleepQualityPercentage) + "</span><div class=\"slice\"><div class= \"bar\"></div><div class=\"fill\"></div></div></div></div></div></div><!--Card Body--><div id=" + cardCollapse + " class=\"collapse\" aria-labelledby=" + cardHeading + " data-parent=\"#accordion\"><div class=\"card-body\"><div class=\"row\"><!--Graph Canvas--><div class = \"col-lg-8\"><div id=graphContainer" + canvasID + "><canvas class=canvas id=\"" + canvasID + "\"></canvas></div><!--Graph Metrics--><div id=\"playerMetrics\" class=\"col-lg-12\"><button class=playerWellnessButton id=\"fatigue" + cardHeading + canvasID + "\">Fatigue</button><button class=playerWellnessButton id=\"Soreness" + cardHeading + canvasID + "\">Soreness</button><button class=playerWellnessButton class=playerWellnessButton id=\"Mood" + cardHeading + canvasID + "\">Mood</button><button class=playerWellnessButton id=\"Sleep" + cardHeading + canvasID + "\">Sleep</button><button class=playerWellnessButton id=\"Stress" + cardHeading + canvasID + "\">Stress</button><button class=playerWellnessButton id=\"HeartRate" + cardHeading + canvasID + "\">Heart Rate</button><br><button class=playerWellnessButton id=\"seven" + cardHeading + canvasID + "\">7 days</button><button class=playerWellnessButton id=\"thirty" + cardHeading + canvasID + "\">30 days</button><button class=playerWellnessButton id=\"sixty" + cardHeading + canvasID + "\">60 days </button><button class=playerWellnessButton id=\"ninety" + cardHeading + canvasID + "\">90 days</button></div></div><!--total Wellness Circle--><div class=\"col-lg-4\"><div class=\"row\"><div class=\"col-lg-12\"><div style=\"height:132px;\" id=\"pcMed\"><div class=\"c100 p"+ wellnessGrade +" " + wellnessColor + "\"><span>"+ wellnessGrade + percentSymbol(wellnessGrade) + "</span><div class=\"slice\"><div class= \"bar\"></div><div class=\"fill\"></div></div></div></div><h5 id=\"circleName\">Total Wellness</h5></div></div><!--Mental/Physical Wellness Circles--><div class=\"row\"><div class=\"col-lg-6\"><div style=\"height:90px\" id=pcSmall><div class=\"c100 p" + physicalReadiness + " " + physicalReadinessColor + " small\"><span>" + physicalReadiness + percentSymbol(physicalReadiness) + "</span><div class=\"slice\"><div class= \"bar\"></div><div class=\"fill\"></div></div></div></div><h6 id=\"circleName\">Physical Wellness</h6></div><div class=\"col-lg-6\"><div style=\"height:90px\" id=\"pcSmall\"><div class=\"c100 p" + mentalReadiness + " " + mentalReadinessColor + " small\"><span>" + mentalReadiness + percentSymbol(mentalReadiness) + "</span><div class=\"slice\"><div class= \"bar\"></div><div class=\"fill\"></div></div></div></div><h6 id=\"circleName\">Mental Wellness</h6></div></div></div></div>"
                $("#accordion").append(card);

                if (fatigue != null && soreness != null && mood != null && sleep != null && stress != null){
                  if ((fatigue + soreness + mood + sleep + stress) < 16) {
                    $("#" + cardHeading).addClass("bg-danger");
                    $("#" + buttonID).css("color", "white");
                  }
                }

                selectedValue.push("fatigue"); // set starting value for each as fatigue

                // sets up the graph for each card
                // set up initially to be for 7 days
                setUpGraph(cardHeading, canvasID, teamCode, snapshot, dateSortedSeven, teamDateSortedSeven, 7);

                toggleButtonDateRange('seven', $('#'+'seven' + cardHeading + canvasID)[0]);
                toggleButtonWellness('fatigue', $('#'+'fatigue'+ cardHeading + canvasID)[0]);

                //expand graph to seven day view
                document.getElementById('seven' + cardHeading + canvasID).addEventListener('click', function() {
                  removeListeners(cardHeading, canvasID);
                  setUpGraph(cardHeading, canvasID, teamCode, snapshot, dateSortedSeven, teamDateSortedSeven, 7);
                  toggleButtonDateRange('seven', this);
                });
                //expand graph to thirty day view
                document.getElementById('thirty' + cardHeading + canvasID).addEventListener('click', function() {
                  removeListeners(cardHeading, canvasID);
                  setUpGraph(cardHeading, canvasID, teamCode, snapshot, dateSortedThirty, teamDateSortedThirty, 30);
                  toggleButtonDateRange('thirty', this);
                });
                //expand graph to sixty day view
                document.getElementById('sixty' + cardHeading + canvasID).addEventListener('click', function() {
                  removeListeners(cardHeading, canvasID);
                  setUpGraph(cardHeading, canvasID, teamCode, snapshot, dateSortedSixty, teamDateSortedSixty, 60);
                  toggleButtonDateRange('sixty', this);
                });
                //expand graph to ninety day view
                document.getElementById('ninety' + cardHeading + canvasID).addEventListener('click', function() {
                  removeListeners(cardHeading, canvasID);
                  setUpGraph(cardHeading, canvasID, teamCode, snapshot, dateSortedNinety, teamDateSortedNinety, 90, count - 1);
                  toggleButtonDateRange('ninety', this);
                });

                count++;
              }
            });
          }
        }
      });
    })
    .catch(function(err){
      console.log(err);
    });
});

// removes listeners from teamValues
function removeListeners(heading, id){
  $("#fatigue" + heading + id).off();
  $("#Soreness" + heading + id).off();
  $("#Mood" + heading + id).off();
  $("#Stress" + heading + id).off();
  $("#HeartRate" + heading + id).off();
  $("#Sleep" + heading + id).off();
}

// add or do not add percent symbol
function percentSymbol(value){
  if (value == "N/A"){
    return ""
  }
  return "%"
}

// calculate percentage for each value
function calculatePercentage(value){
  if (value != null){
    return Math.floor((value/5)*100);
  } else {
    return "N/A"
  }
}

// return correct color coding value
function calculateColorForValues(value){
  if (value == 5){
    return "darkgreen";
  }else if (value == 4){
    return "yellowgreen";
  }else if (value == 3){
    return "yellow";
  }else if (value == 2){
    return "orange";
  }else if (value == 1){
    return "red";
  }else{
    return "gray";
  }
}

// calculate percentages
function calculatePercentageForSuperScores(value){
  if (value == null){
    return "N/A";
  }
  return Math.floor(value);
}

// return correct color based on percentage values
function calculateColorForPercentage(percentage){
  if(percentage >= 80){
    return "darkgreen";
  }else if(percentage >= 60 && percentage < 80){
    return "yellowgreen";
  }else if (percentage < 60 && percentage >= 40){
    return "yellow";
  }else if (percentage < 40 && percentage >= 20){
    return "orange";
  } else if (percentage < 20 && percentage >= 0){
    return "red";
  }else if (percentage == null){
    return "gray";
  }
}

// makes sure the user is logged in
function checkIfLoggedIn(){
  // Setup the log out button
  document.getElementById("tempLogoutButton").addEventListener("click", function(){
    firebase.auth().signOut()
  });
  return new Promise(function (resolve, reject) {
    // establish if the user is there
    firebase.auth().onAuthStateChanged(firebaseUser => {
      // Check if the user is logged in or not
      if(firebaseUser){
        // Make the page visible since they are logged in
        $("#body").removeAttr("hidden");
        uid = firebase.auth().currentUser.uid;
        return resolve();
      }else{
        // The user logged out
        // Clear sessionStorage and redirect them to login screen
        sessionStorage.clear();
        location.replace("../SignUp/index.html");
        return reject("User is not logged in");
      }
    });
  });
}

// retreives team code
function getTeamCode(){
  return new Promise(function (resolve, reject) {
    try {
      if(typeof(Storage) !== "undefined") {

        // First check if they have a coachName stored in sessionStorage
        // If they don't, we are going to want to retrieve it from firebase
        if(sessionStorage.coachesName){
          coachesName = sessionStorage.coachesName;
        }else{
          throw "no coachName";
        }

        // Then check if they have a teamCode stored in sessionStorage
        if(sessionStorage.teamCode){
          return resolve(sessionStorage.teamCode);
        }else{
          throw "no teamcode";
        }
      }else {
        console.log("Sorry, your browser does not support web storage...");
        throw "no teamcode";
      }
    }catch(e){
      // Our session didn't have the teamCode
      // Will have to manually retrieve the teamcode from firebase
      var uid = firebase.auth().currentUser.uid;
      const retrieveTeamCode = firebase.database().ref("coaches").child(firebase.auth().currentUser.uid);
      retrieveTeamCode.once('value').then(function(snapshot){
        var coachInfo = snapshot.val();
        if(coachInfo !== null){
          if(coachInfo.hasOwnProperty('name')){
            var lastName = coachInfo['name'].split(" ").pop();
            if(typeof(Storage) !== "undefined") {
              sessionStorage.coachesName = lastName;
            }
            coachesName = lastName
          }else{
            console.log("settings.js :: getTeamCode :: No coach name exists");
          }

          if(coachInfo.hasOwnProperty('team')){
            if(typeof(Storage) !== "undefined") {
              sessionStorage.teamCode = coachInfo['team'];
            }
            return resolve(coachInfo['team']);
          }
        }
        return reject("No team code exists for this coach");
      });
    }
  });
}

// returns the date based on how many days you want to go back
function retreivePastDateBasedOnNumberofDays(value){
  var date = new Date();
  // https://stackoverflow.com/questions/19910161/javascript-calculating-date-from-today-date-to-7-days-before
  var last = new Date(date.getTime() - ((value - 1) * 24 * 60 * 60 * 1000));
  var date = last.getDate();
  var monthIndex = last.getMonth() + 1;
  var year = last.getFullYear();

  if (date < 10){
    date = "0" + date;
  }

  if (monthIndex < 10){
   monthIndex = "0" + monthIndex;
  }

  return  monthIndex + "-" +  date + "-" +year;
}

// https://stackoverflow.com/questions/3593934/how-do-i-create-an-array-of-points
function Point(x, y) {
  this.x = x;
  this.y = y;
}

// gets the values from the snapshot based on fatigue, mood, soreness, etc
function calculateValuesForGraph(numberOfDays, snapshot, type){
  var values = snapshot.val();

  // creates a map where the key is the date and the value is the value of type for that day
  var mapForValues = Object();
  for (var date in values){
    mapForValues[date] = snapshot.child(date).child("WQ").child(type).val();
  }

  // calculates the last numberOfDays and set map to null
  var past_days_values = Object()
  for (i = (numberOfDays - 1); i >= 0; i--) {
    var date = new Date();
    // https://stackoverflow.com/questions/19910161/javascript-calculating-date-from-today-date-to-7-days-before
    var last = new Date(date.getTime() - (i * 24 * 60 * 60 * 1000));
    var date = last.getDate();
    var monthIndex = last.getMonth() + 1;
    var year = last.getFullYear();

    if (date < 10){
      date = "0" + date;
    }

    if (monthIndex < 10){
     monthIndex = "0" + monthIndex;
    }

    var date = monthIndex + "-" +  date + "-" +year;

    past_days_values[date.toString()] = null;
  }

  // type and set key to correct values
  Object.keys(past_days_values).forEach(function(key) {
    past_days_values[key] = mapForValues[key]
  });

  // set up array
  var data = [];
  Object.keys(past_days_values).forEach(function(key) {
      value = past_days_values[key]
      if (value){
        if (type != "heartRate") {
          value = (value / 5) * 100;
        }
      }

      data.push(value)
  });

  return data;

}

// gets the values from the snapshot for the Team average based on avgFatigue, avgMood, avgSoreness, etc
function teamCalculateValuesForGraph(numberOfDays, snapshot, type){
  var values = snapshot.val();
  // creates a map where the key is the date and the value is the value of fatigue for that day
  var teamValues = Object();
  for (var date in values){
    teamValues[date] = snapshot.child(date).child("averages").child(type).val();
  }

  // calculates the last numberOfDays and set map to null
  var team_past_days_value = Object()
  for (i = (numberOfDays - 1); i >= 0; i--) {
    var date = new Date();
    // https://stackoverflow.com/questions/19910161/javascript-calculating-date-from-today-date-to-7-days-before
    var last = new Date(date.getTime() - (i * 24 * 60 * 60 * 1000));
    var date = last.getDate();
    var monthIndex = last.getMonth() + 1;
    var year = last.getFullYear();

    if (date < 10){
      date = "0" + date;
    }

    if (monthIndex < 10){
     monthIndex = "0" + monthIndex;
    }

    var date = monthIndex + "-" +  date + "-" +year;

    team_past_days_value[date.toString()] = null; // sentinel value
  }

  // Team value
  Object.keys(team_past_days_value).forEach(function(key) {
    team_past_days_value[key] = teamValues[key]
  });

  var data = [];
  Object.keys(team_past_days_value).forEach(function(key) {
    value = team_past_days_value[key]
    if (type != "avgHeartRate") {
      value = (value / 5) * 100;
    }
    data.push(value)
  });

  return data;
}

// retreive all dates in an array from the past number of listed days
function retreiveMultiplePastDateBasedOnNumberofDays(numberOfDays) {
  var days_label = [];
  for (i = (numberOfDays - 1); i >= 0; i--) {
      var date = new Date();
      // https://stackoverflow.com/questions/19910161/javascript-calculating-date-from-today-date-to-7-days-before
      var last = new Date(date.getTime() - (i * 24 * 60 * 60 * 1000));
      var date = last.getDate();
      var monthIndex = last.getMonth() + 1;
      var year = last.getFullYear();

      if (date < 10){
        date = "0" + date;
      }

      if (monthIndex < 10){
       monthIndex = "0" + monthIndex;
      }

      var d = monthIndex + "-" +  date + "-" +year;

      days_label.push(d.toString());
    }
    return days_label;
}

function setUpGraph(cardHeading, canvasID, teamCode, snapshot, dataRef, teamDataRef, numberOfDays) {
  // retreive all sorted dates based on how far back the coach wants to go
    dataRef.once('value').then(function(snapshot){

      var count = parseInt(cardHeading.replace('heading',''));
      var days_label = retreiveMultiplePastDateBasedOnNumberofDays(numberOfDays);

      var fatigueData = calculateValuesForGraph(numberOfDays, snapshot, "fatigue");
      var sorenessData = calculateValuesForGraph(numberOfDays, snapshot, "soreness");
      var moodData = calculateValuesForGraph(numberOfDays, snapshot, "mood");
      var sleepQualityData = calculateValuesForGraph(numberOfDays, snapshot, "sleepQuality");
      var stressData = calculateValuesForGraph(numberOfDays, snapshot, "stress");
      var heartRateData = calculateValuesForGraph(numberOfDays, snapshot, "heartRate");

      // grab average team grades to run against
      teamDataRef.once('value').then(function(snapshot){
        var teamFatigueData = teamCalculateValuesForGraph(numberOfDays, snapshot, "avgFatigue")
        var teamSorenessData = teamCalculateValuesForGraph(numberOfDays, snapshot, "avgSoreness")
        var teamMoodData = teamCalculateValuesForGraph(numberOfDays, snapshot, "avgMood")
        var teamSleepQualityData = teamCalculateValuesForGraph(numberOfDays, snapshot, "avgSleepQuality")
        var teamStressData = teamCalculateValuesForGraph(numberOfDays, snapshot, "avgStress")
        var teamHeartRateData = teamCalculateValuesForGraph(numberOfDays, snapshot, "avgHeartRate")

        // set up all lines to be hidden
        var fatigueHidden = true;
        var teamFatigueHidden = true;
        var sorenessHidden = true;
        var teamSorenessHidden = true;
        var moodHidden = true;
        var teamMoodHidden = true;
        var sleepHidden = true;
        var teamSleepHidden = true;
        var stressHidden = true;
        var teamStressHidden = true;
        var heartRateHidden = true;
        var teamHeartRateHidden = true;

        // select which parts of the graph to show based on what was already selected
        switch (selectedValue[count - 1]) {
          case "fatigue":
            fatigueHidden = false;
            teamFatigueHidden = false;
            break;
          case "soreness":
            sorenessHidden = false;
            teamSorenessHidden = false;
            break;
          case "mood":
            moodHidden = false;
            teamMoodHidden = false;
            break;
          case "sleep":
            sleepHidden = false;
            teamSleepHidden = false;
            break;
          case "stress":
            stressHidden = false;
            teamStressHidden = false;
            break;
          case "heartRate":
            heartRateHidden = false;
            teamHeartRateHidden = false;
            break;
          case "none":
            break;
        }

        var config = {
            type: 'line',
            data: {
                labels: days_label,
                datasets: [{
                    label: "Player Fatigue",
                    backgroundColor: window.chartColors.red,
                    borderColor: window.chartColors.red,
                    data: fatigueData,
                    fill: false,
                    lineTension:0,
                    spanGaps: true,
                    hidden: fatigueHidden
                },
                {
                    label: "Team Average Fatigue",
                    backgroundColor: window.chartColors.gray,
                    borderColor: window.chartColors.gray,
                    data: teamFatigueData,
                    fill: false,
                    lineTension:0,
                    spanGaps: true,
                    hidden: teamFatigueHidden
                },
                {
                    label: "Player Soreness",
                    backgroundColor: window.chartColors.blue,
                    borderColor: window.chartColors.blue,
                    data: sorenessData,
                    fill: false,
                    lineTension:0,
                    spanGaps: true,
                    hidden: sorenessHidden
                },
                {
                    label: "Team Average Soreness",
                    backgroundColor: window.chartColors.gray,
                    borderColor: window.chartColors.gray,
                    data: teamSorenessData,
                    fill: false,
                    lineTension:0,
                    spanGaps: true,
                    hidden: teamSorenessHidden
                },
                {
                    label: "Player Mood",
                    backgroundColor: window.chartColors.green,
                    borderColor: window.chartColors.green,
                    data: moodData,
                    fill: false,
                    lineTension:0,
                    spanGaps: true,
                    hidden: moodHidden
                },
                {
                    label: "Team Average Mood",
                    backgroundColor: window.chartColors.gray,
                    borderColor: window.chartColors.gray,
                    data: teamMoodData,
                    fill: false,
                    lineTension:0,
                    spanGaps: true,
                    hidden: teamMoodHidden
                },
                {
                    label: "Sleep Quality",
                    backgroundColor: window.chartColors.yellow,
                    borderColor: window.chartColors.yellow,
                    data: sleepQualityData,
                    fill: false,
                    lineTension:0,
                    spanGaps: true,
                    hidden: sleepHidden
                },
                {
                    label: "Team Average Sleep Quality",
                    backgroundColor: window.chartColors.gray,
                    borderColor: window.chartColors.gray,
                    data: teamSleepQualityData,
                    fill: false,
                    lineTension:0,
                    spanGaps: true,
                    hidden: teamSleepHidden
                },
                {
                    label: "Player Stress",
                    backgroundColor: window.chartColors.purple,
                    borderColor: window.chartColors.purple,
                    data: stressData,
                    fill: false,
                    lineTension:0,
                    spanGaps: true,
                    hidden: stressHidden
                },
                {
                    label: "Team Average Stress",
                    backgroundColor: window.chartColors.gray,
                    borderColor: window.chartColors.gray,
                    data: teamStressData,
                    fill: false,
                    lineTension:0,
                    spanGaps: true,
                    hidden: teamStressHidden
                },
                {
                    label: "Player Heart Rate",
                    backgroundColor: 'rgba(255,192,203,1)',
                    borderColor: 'rgba(255,192,203,1)',
                    data: heartRateData,
                    fill: false,
                    lineTension:0,
                    spanGaps: true,
                    hidden: heartRateHidden
                },
                {
                    label: "Team Average Heart Rate",
                    backgroundColor: window.chartColors.gray,
                    borderColor: window.chartColors.gray,
                    data: teamHeartRateData,
                    fill: false,
                    lineTension:0,
                    spanGaps: true,
                    hidden: teamHeartRateHidden
                }
              ]
            },
            //display options for graph
            options: {
              responsive: true,
              tooltips: {
                  mode: 'index',
                  ntersect: false,
              },
              legend:{
                  display: false
              },
              hover: {
                  mode: 'nearest',
                  intersect: true
              },
              scales: {
                  xAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Time'
                    }
                  }],
                  yAxes: [{
                    display: true,
                    scaleLabel: {
                      display: true,
                      labelString: 'Value'
                    }
                  }]
              }
           }
        };

        $('#' + canvasID).remove(); // this is my <canvas> element
        $('#graphContainer' + canvasID).append('<canvas id=' + canvasID + '><canvas>');

        var ctx = document.getElementById(canvasID);
        var chart = new Chart(ctx, config);

        $("#fatigue" + cardHeading + canvasID).click(function(){
          if (chart.config.data.datasets[0].hidden){
            //Toggle button
            //$(this).addClass("selectedDateRangeButton");
            toggleButtonWellness("fatigue", this);
            // hide everything
            for (var i = 0; i < chart.config.data.datasets.length; i++) {
              chart.config.data.datasets[i].hidden = true;
            }

            chart.config.data.datasets[0] = {
                label: "Player Fatigue",
                backgroundColor: window.chartColors.red,
                borderColor: window.chartColors.red,
                data: fatigueData,
                fill: false,
                lineTension:0,
                spanGaps: true
            };

            chart.config.data.datasets[1] = {
                label: "Team Average Fatigue",
                backgroundColor: window.chartColors.gray,
                borderColor: window.chartColors.gray,
                data: teamFatigueData,
                fill: false,
                lineTension:0,
                spanGaps: true
            };
            selectedValue[count] = "fatigue";
          } else {
            // hide everything
            //means you clicked it again. untoggle
            $(this).removeClass("selectedDateRangeButton");
            for (var i = 0; i < chart.config.data.datasets.length; i++) {
              chart.config.data.datasets[i].hidden = true;
            }
            selectedValue[count] = "none";
          }
          chart.update();

        });


        //display soreness on the graph
        $("#Soreness" + cardHeading + canvasID).click(function(){
          if (chart.config.data.datasets[2].hidden){
            // hide everything
            toggleButtonWellness("Soreness", this);
            for (var i = 0; i < chart.config.data.datasets.length; i++) {
              chart.config.data.datasets[i].hidden = true;
            }
            chart.config.data.datasets[2] = {
                label: "Player Soreness",
                backgroundColor: window.chartColors.blue,
                borderColor: window.chartColors.blue,
                data: sorenessData,
                fill: false,
                lineTension:0,
                spanGaps: true,
            };

            chart.config.data.datasets[3] = {
                label: "Team Average Soreness",
                backgroundColor: window.chartColors.gray,
                borderColor: window.chartColors.gray,
                data: teamSorenessData,
                fill: false,
                lineTension:0,
                spanGaps: true
            };
            selectedValue[count] = "soreness";
          } else {
            // hide everything
            $(this).removeClass("selectedDateRangeButton");
            for (var i = 0; i < chart.config.data.datasets.length; i++) {
              chart.config.data.datasets[i].hidden = true;
            }
            selectedValue[count] = "none";
          }
          chart.update();
        });
        //display mood on the graph
        $("#Mood" + cardHeading + canvasID).click(function(){
          if (chart.config.data.datasets[4].hidden){
            // hide everything
            toggleButtonWellness("Mood", this);
            for (var i = 0; i < chart.config.data.datasets.length; i++) {
              chart.config.data.datasets[i].hidden = true;
            }
            chart.config.data.datasets[4] = {
                label: "Player Mood",
                backgroundColor: window.chartColors.green,
                borderColor: window.chartColors.green,
                data: moodData,
                fill: false,
                lineTension:0,
                spanGaps: true,
            };
            chart.config.data.datasets[5] = {
                label: "Team Average Mood",
                backgroundColor: window.chartColors.gray,
                borderColor: window.chartColors.gray,
                data: teamMoodData,
                fill: false,
                lineTension:0,
                spanGaps: true
            };
            selectedValue[count] = "mood";
          } else {
            // hide everything
            $(this).removeClass("selectedDateRangeButton");
            for (var i = 0; i < chart.config.data.datasets.length; i++) {
              chart.config.data.datasets[i].hidden = true;
            }
            selectedValue[count] = "none";
          }

          chart.update();
        });
        //display sleep quality on the graph
        $("#Sleep" + cardHeading + canvasID).click(function(){
          if (chart.config.data.datasets[6].hidden){
            // hide everything
            toggleButtonWellness("Sleep", this);
            for (var i = 0; i < chart.config.data.datasets.length; i++) {
              chart.config.data.datasets[i].hidden = true;
            }
            chart.config.data.datasets[6] = {
                label: "Player Sleep",
                backgroundColor: window.chartColors.yellow,
                borderColor: window.chartColors.yellow,
                data: sleepQualityData,
                fill: false,
                lineTension:0,
                spanGaps: true,
            };
            chart.config.data.datasets[7] = {
                label: "Team Average Sleep",
                backgroundColor: window.chartColors.gray,
                borderColor: window.chartColors.gray,
                data: teamSleepQualityData,
                fill: false,
                lineTension:0,
                spanGaps: true,
            };
            selectedValue[count] = "sleep";
          } else {
            // hide everything
            $(this).removeClass("selectedDateRangeButton");
            for (var i = 0; i < chart.config.data.datasets.length; i++) {
              chart.config.data.datasets[i].hidden = true;
            }
            selectedValue[count] = "none";
          }

          chart.update();
        });
        //display sleep quality on the graph
        $("#Stress" + cardHeading + canvasID).click(function(){
          if (chart.config.data.datasets[8].hidden){
            // hide everything
            toggleButtonWellness("Stress", this);
            for (var i = 0; i < chart.config.data.datasets.length; i++) {
              chart.config.data.datasets[i].hidden = true;
            }
            chart.config.data.datasets[8] = {
                label: "Player Stress",
                backgroundColor: window.chartColors.purple,
                borderColor: window.chartColors.purple,
                data: stressData,
                fill: false,
                lineTension:0,
                spanGaps: true,
            };
            chart.config.data.datasets[9] = {
                label: "Team Average Stress",
                backgroundColor: window.chartColors.gray,
                borderColor: window.chartColors.gray,
                data: teamStressData,
                fill: false,
                lineTension:0,
                spanGaps: true,
            };
            selectedValue[count] = "stress";
          } else {
            // hide everything
            $(this).removeClass("selectedDateRangeButton");
            for (var i = 0; i < chart.config.data.datasets.length; i++) {
              chart.config.data.datasets[i].hidden = true;
            }
            selectedValue[count] = "none";
          }

          chart.update();
        });
        //display sleep quality on the graph
        $("#HeartRate" + cardHeading + canvasID).click(function(){
          if (chart.config.data.datasets[10].hidden){
            toggleButtonWellness("HeartRate", this);
            // hide everything
            for (var i = 0; i < chart.config.data.datasets.length; i++) {
              chart.config.data.datasets[i].hidden = true;
            }
            chart.config.data.datasets[10] = {
                label: "Player Heart Rate",
                backgroundColor: 'rgba(255,192,203,1)',
                borderColor: 'rgba(255,192,203,1)',
                data: heartRateData,
                fill: false,
                lineTension:0,
                spanGaps: true,
            };
            chart.config.data.datasets[11] = {
                label: "Team Average Heart Rate",
                backgroundColor: window.chartColors.gray,
                borderColor: window.chartColors.gray,
                data: teamHeartRateData,
                fill: false,
                lineTension:0,
                spanGaps: true,
            };
            selectedValue[count] = "heartRate";
          } else {
            // hide everything
            $(this).removeClass("selectedDateRangeButton");
            for (var i = 0; i < chart.config.data.datasets.length; i++) {
              chart.config.data.datasets[i].hidden = true;
            }
            selectedValue[count] = "none";
          }
          chart.update();
        });
        return chart;
      });
    });
  }

  // format to key format
  // https://stackoverflow.com/questions/3552461/how-to-format-a-javascript-date
  function formatDate(date) {
    var day = date.getDate();
    var monthIndex = date.getMonth() + 1; // zero based index
    var year = date.getFullYear();

    if (day < 10){
      day = "0" + day;
    }

    if (monthIndex < 10){
     monthIndex = "0" + monthIndex;
    }

    return monthIndex + '-' + day + '-' + year;
  }

  // Changes the background of a date picker button for the graph when clicked
  function toggleButtonDateRange(selectedDateRange, button){
    var dateRanges = ["seven", "thirty", "sixty", "ninety"];
    for (var i = 0; i < dateRanges.length; i++) {
      // set all of the other button's backgrounds back to unselected color
      if(dateRanges[i] !== selectedDateRange){
        // Replace selectedDateRange with dateRanges[i]
        var unselectedButton = button.id.replace(selectedDateRange, dateRanges[i]);
        $('#'+ unselectedButton ).removeClass("selectedDateRangeButton");
      }
    }
    // Grab our selected button in jquery and add a class
    $('#'+button.id).addClass("selectedDateRangeButton");
  }

  // Changes the background of a wellness state button for the graph when clicked
  function toggleButtonWellness(wellnessStat, button){
    var states = ["fatigue","Soreness","Mood","Sleep","Stress","HeartRate"];
    for (var i = 0; i < states.length; i++) {
      // set all of the other button's backgrounds back to unselected color
      if(states[i] !== wellnessStat){
        // Replace selectedDateRange with dateRanges[i]
        var unselectedButton = button.id.replace(wellnessStat, states[i]);
        $('#'+ unselectedButton ).removeClass("selectedDateRangeButton");
      }
    }
    // Grab our selected button in jquery and add a class
    $('#'+button.id).addClass("selectedDateRangeButton");
  }
