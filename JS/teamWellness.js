/*
 *
 * Created by Samuel Kenney
 *
 * Copyright © 2018 Le Crew Studios. All Rights Reserved.
 *
 */
var selectedValue = []; // keeps track of the selected values so transition to new date span holds what was selected
var singletonChart;
var coachesName;

$(document).ready(function() {
  // check if the user is logged in
  checkIfLoggedIn()
    .then(function() {
      // if they are logged in, get the code
      return getTeamCode();
    })
    .then(function(teamCode) {

      // Insert coachName into HTML
      if(coachesName){
        $('#coachName').html("Coach " + coachesName);
      }

      // Set up database references for graph that will be used later
      var pastDateSeven = retreivePastDateBasedOnNumberofDays(7);
      var pastDateThirty = retreivePastDateBasedOnNumberofDays(30);
      var pastDateSixty = retreivePastDateBasedOnNumberofDays(60);
      var pastDateNinety = retreivePastDateBasedOnNumberofDays(90);

      const teamDateSortedSeven = firebase.database().ref("teams/" +
        teamCode + "/wellnessData").orderByKey().startAt(pastDateSeven);
      const teamDateSortedThirty = firebase.database().ref("teams/" +
        teamCode + "/wellnessData").orderByKey().startAt(pastDateThirty);
      const teamDateSortedSixty = firebase.database().ref("teams/" +
        teamCode + "/wellnessData").orderByKey().startAt(pastDateSixty);
      const teamDateSortedNinety = firebase.database().ref("teams/" +
        teamCode + "/wellnessData").orderByKey().startAt(pastDateNinety);

      selectedValue.push("fatigue"); // make sure it sets back to start on fatigue
      // sets up the graph for each card
      // set up initially to be for 7 days
      setUpGraph(teamCode, teamDateSortedSeven, 7);
      toggleButtonDateRange('seven', $('#'+'seven')[0]);
      toggleButtonWellness($('#'+'Fatigue')[0]);

      // start fatigue off as the selected one
      // TODO: Figure out how to toggle buttons
      //$("#fatigue" + cardHeading + "canvasTeamGraph").addClass("selected");
      //$("#seven" + cardHeading + "canvasTeamGraph").addClass("selected");

      //expand graph to seven day view
      document.getElementById('seven').addEventListener(
        'click',
        function() {
          removeListeners();
          setUpGraph(teamCode, teamDateSortedSeven, 7);
          toggleButtonDateRange('seven', this);
        });
      //expand graph to thirty day view
      document.getElementById('thirty').addEventListener(
        'click',
        function() {
          removeListeners();
          setUpGraph(teamCode, teamDateSortedThirty, 30);
          toggleButtonDateRange('thirty', this);
        });
      //expand graph to sixty day view
      document.getElementById('sixty').addEventListener(
        'click',
        function() {
          removeListeners();
          setUpGraph(teamCode, teamDateSortedSixty, 60);
          toggleButtonDateRange('sixty', this);
        });
      //expand graph to ninety day view
      document.getElementById('ninety').addEventListener(
        'click',
        function() {
          removeListeners();
          setUpGraph(teamCode, teamDateSortedNinety, 90);
          toggleButtonDateRange('ninety', this);
        });
    })
    .catch(function(err) {
      console.log(err);
    });
});

// removes listeners from teamValues
function removeListeners(){
  $("#Fatigue").off();
  $("#Soreness").off();
  $("#Mood").off();
  $("#Stress").off();
  $("#HeartRate").off();
  $("#Sleep").off();
  $("#wellnessGrade").off();
  $("#mentalReadiness").off();
  $("#physicalReadiness").off();
}

// makes sure the user is logged in
function checkIfLoggedIn() {
  // Setup the log out button
  document.getElementById("tempLogoutButton").addEventListener("click",
    function() {
      firebase.auth().signOut()
    });
  return new Promise(function(resolve, reject) {
    // establish if the user is there
    firebase.auth().onAuthStateChanged(firebaseUser => {
      // Check if the user is logged in or not
      if (firebaseUser) {
        // Make the page visible since they are logged in
        $("#body").removeAttr("hidden");
        uid = firebase.auth().currentUser.uid;
        return resolve();
      } else {
        // The user logged out
        // Clear sessionStorage and redirect them to login screen
        sessionStorage.clear();
        location.replace("SignUp/index.html");
        return reject("User is not logged in");
      }
    });
  });
}

// retreives team code
function getTeamCode(){
  return new Promise(function (resolve, reject) {
    try {
      if(typeof(Storage) !== "undefined") {

        // First check if they have a coachName stored in sessionStorage
        // If they don't, we are going to want to retrieve it from firebase
        if(sessionStorage.coachesName){
          coachesName = sessionStorage.coachesName;
        }else{
          throw "no coachName";
        }

        // Then check if they have a teamCode stored in sessionStorage
        if(sessionStorage.teamCode){
          return resolve(sessionStorage.teamCode);
        }else{
          throw "no teamcode";
        }
      }else {
        console.log("Sorry, your browser does not support web storage...");
        throw "no teamcode";
      }
    }catch(e){
      // Our session didn't have the teamCode
      // Will have to manually retrieve the teamcode from firebase
      var uid = firebase.auth().currentUser.uid;
      const retrieveTeamCode = firebase.database().ref("coaches").child(firebase.auth().currentUser.uid);
      retrieveTeamCode.once('value').then(function(snapshot){
        var coachInfo = snapshot.val();
        if(coachInfo !== null){
          if(coachInfo.hasOwnProperty('name')){
            var lastName = coachInfo['name'].split(" ").pop();
            sessionStorage.coachesName = lastName;
            coachesName = lastName
          }else{
            console.log("teamWellness.js :: getTeamCode :: No coach name exists");
          }

          if(coachInfo.hasOwnProperty('team')){
            sessionStorage.teamCode = coachInfo['team'];
            return resolve(coachInfo['team']);
          }
        }
        return reject("No team code exists for this coach");
      });
    }
  });
}

// returns the date based on how many days you want to go back
function retreivePastDateBasedOnNumberofDays(value) {
  var date = new Date();
  // https://stackoverflow.com/questions/19910161/javascript-calculating-date-from-today-date-to-7-days-before
  var last = new Date(date.getTime() - ((value - 1) * 24 * 60 * 60 * 1000));
  var date = last.getDate();
  var monthIndex = last.getMonth() + 1;
  var year = last.getFullYear();

  if (date < 10) {
    date = "0" + date;
  }

  if (monthIndex < 10) {
    monthIndex = "0" + monthIndex;
  }

  return monthIndex + "-" + date + "-" + year;
}

// https://stackoverflow.com/questions/3593934/how-do-i-create-an-array-of-points
function Point(x, y) {
  this.x = x;
  this.y = y;
}

// gets the values from the snapshot for the Team average based on avgFatigue, avgMood, avgSoreness, etc
function teamCalculateValuesForGraph(numberOfDays, snapshot, type) {
  var values = snapshot.val();
  // creates a map where the key is the date and the value is the value of fatigue for that day
  var teamValues = Object();
  for (var date in values) {
    teamValues[date] = snapshot.child(date).child("averages").child(type)
      .val();
  }

  // calculates the last numberOfDays and set map to null
  var team_past_days_value = Object()
  for (i = (numberOfDays - 1); i >= 0; i--) {
    var date = new Date();
    // https://stackoverflow.com/questions/19910161/javascript-calculating-date-from-today-date-to-7-days-before
    var last = new Date(date.getTime() - (i * 24 * 60 * 60 * 1000));
    var date = last.getDate();
    var monthIndex = last.getMonth() + 1;
    var year = last.getFullYear();

    if (date < 10) {
      date = "0" + date;
    }

    if (monthIndex < 10) {
      monthIndex = "0" + monthIndex;
    }

    var date = monthIndex + "-" + date + "-" + year;

    team_past_days_value[date.toString()] = null; // sentinel value
  }

  // Team value
  Object.keys(team_past_days_value).forEach(function(key) {
    team_past_days_value[key] = teamValues[key]
  });

  var data = [];
  Object.keys(team_past_days_value).forEach(function(key) {
    value = team_past_days_value[key];
    if (type != "wellnessGrade" && type != "physicalReadiness" && type !=
      "mentalReadiness" && type != "avgHeartRate") {
      value = (value / 5) * 100;

    }
    data.push(value)
  });

  return data;
}

// retreive all dates in an array from the past number of listed days
function retreiveMultiplePastDateBasedOnNumberofDays(numberOfDays) {
  var days_label = [];
  for (i = (numberOfDays - 1); i >= 0; i--) {
    var date = new Date();
    // https://stackoverflow.com/questions/19910161/javascript-calculating-date-from-today-date-to-7-days-before
    var last = new Date(date.getTime() - (i * 24 * 60 * 60 * 1000));
    var date = last.getDate();
    var monthIndex = last.getMonth() + 1;
    var year = last.getFullYear();

    if (date < 10) {
      date = "0" + date;
    }

    if (monthIndex < 10) {
      monthIndex = "0" + monthIndex;
    }

    var d = monthIndex + "-" + date + "-" + year;

    days_label.push(d.toString());
  }
  return days_label;
}

function setUpGraph(teamCode,
  teamDataRef, numberOfDays) {
  // retreive all sorted dates based on how far back the coach wants to go
  var seven_days_label = retreiveMultiplePastDateBasedOnNumberofDays(
    numberOfDays);

  // grab average team grades to run against
  teamDataRef.once('value').then(function(snapshot) {
    if (singletonChart != null) {
      singletonChart.destroy();
    }

    var teamFatigueData = teamCalculateValuesForGraph(numberOfDays,
      snapshot, "avgFatigue")
    var teamSorenessData = teamCalculateValuesForGraph(numberOfDays,
      snapshot, "avgSoreness")
    var teamMoodData = teamCalculateValuesForGraph(numberOfDays,
      snapshot, "avgMood")
    var teamSleepQualityData = teamCalculateValuesForGraph(
      numberOfDays, snapshot, "avgSleepQuality")
    var teamStressData = teamCalculateValuesForGraph(numberOfDays,
      snapshot, "avgStress")
    var teamHeartRateData = teamCalculateValuesForGraph(numberOfDays,
      snapshot, "avgHeartRate")

    var teamWellnessGrade = teamCalculateValuesForGraph(numberOfDays,
      snapshot, "wellnessGrade")
    var teamMentalReadiness = teamCalculateValuesForGraph(numberOfDays,
      snapshot, "mentalReadiness")
    var teamPhysicalReadiness = teamCalculateValuesForGraph(numberOfDays,
      snapshot, "physicalReadiness")

    // set up all lines to be hidden
    var teamFatigueHidden = true;
    var teamSorenessHidden = true;
    var teamMoodHidden = true;
    var teamSleepHidden = true;
    var teamStressHidden = true;
    var teamHeartRateHidden = true;
    var teamWellnessGradeHidden = true;
    var teamMentalReadinessHidden = true;
    var teamPhysicalReadinessHidden = true;

    // select which parts of the graph to show based on what was already selected
    for (var i = 0; i < selectedValue.length; i++) {
      switch (selectedValue[i]) {
        case "fatigue":
          teamFatigueHidden = false;
          break;
        case "soreness":
          teamSorenessHidden = false;
          break;
        case "mood":
          teamMoodHidden = false;
          break;
        case "sleep":
          teamSleepHidden = false;
          break;
        case "stress":
          teamStressHidden = false;
          break;
        case "heartRate":
          teamHeartRateHidden = false;
          break;
        case "wellnessGrade":
          teamWellnessGradeHidden = false;
          break;
        case "mentalReadiness":
          teamMentalReadinessHidden = false;
          break;
        case "physicalReadiness":
          teamPhysicalReadinessHidden = false;
          break;
        case "none":
          break;
      }
    }

    var config = {
      type: 'line',
      data: {
        labels: seven_days_label,
        datasets: [{
          label: "Fatigue",
          backgroundColor: window.chartColors.red,
          borderColor: window.chartColors.red,
          data: teamFatigueData,
          fill: false,
          lineTension: 0,
          spanGaps: true,
          hidden: teamFatigueHidden
        }, {
          label: "Soreness",
          backgroundColor: window.chartColors.blue,
          borderColor: window.chartColors.blue,
          data: teamSorenessData,
          fill: false,
          lineTension: 0,
          spanGaps: true,
          hidden: teamSorenessHidden
        }, {
          label: "Mood",
          backgroundColor: window.chartColors.green,
          borderColor: window.chartColors.green,
          data: teamMoodData,
          fill: false,
          lineTension: 0,
          spanGaps: true,
          hidden: teamMoodHidden
        }, {
          label: "Sleep Quality",
          backgroundColor: window.chartColors.yellow,
          borderColor: window.chartColors.yellow,
          data: teamSleepQualityData,
          fill: false,
          lineTension: 0,
          spanGaps: true,
          hidden: teamSleepHidden
        }, {
          label: "Stress",
          backgroundColor: window.chartColors.purple,
          borderColor: window.chartColors.purple,
          data: teamStressData,
          fill: false,
          lineTension: 0,
          spanGaps: true,
          hidden: teamStressHidden
        }, {
          label: "Heart Rate",
          backgroundColor: 'rgba(255,192,203,1)',
          borderColor: 'rgba(255,192,203,1)',
          data: teamHeartRateData,
          fill: false,
          lineTension: 0,
          spanGaps: true,
          hidden: teamHeartRateHidden
        }, {
          label: "Team Wellness Grade",
          backgroundColor: 'rgba(34,139,34,1)',
          borderColor: 'rgba(34,139,34,1)',
          data: teamWellnessGrade,
          fill: false,
          lineTension: 0,
          spanGaps: true,
          hidden: teamWellnessGradeHidden
        }, {
          label: "Mental Readiness",
          backgroundColor: 'rgba(238, 130, 238,1)',
          borderColor: 'rgba(238, 130, 238,1)',
          data: teamMentalReadiness,
          fill: false,
          lineTension: 0,
          spanGaps: true,
          hidden: teamMentalReadinessHidden
        }, {
          label: "Phyiscal Readiness",
          backgroundColor: 'rgba(255, 165, 0, 1)',
          borderColor: 'rgba(255, 165, 0, 1)',
          data: teamPhysicalReadiness,
          fill: false,
          lineTension: 0,
          spanGaps: true,
          hidden: teamPhysicalReadinessHidden
        }]
      },
      //display options for graph
      options: {
        responsive: true,
        title: {
          display: true,
        },
        tooltips: {
          mode: 'index',
          ntersect: false,
        },
        legend: {
          display: true
        },
        hover: {
          mode: 'nearest',
          intersect: true
        },
        scales: {
          xAxes: [{
            display: true,
            scaleLabel: {
              display: true,
              labelString: 'Time'
            }
          }],
          yAxes: [{
            display: true,
            scaleLabel: {
              display: true,
              labelString: 'Value'
            }
          }]
        }
      }
    };

    var ctx = document.getElementById("canvasTeamGraph");
    var chart = new Chart(ctx, config);
    singletonChart = chart;

    $("#Fatigue").click(function() {
        if (singletonChart != null) {
          singletonChart.destroy();
        }
        ctx = document.getElementById("canvasTeamGraph");
        chart = new Chart(ctx, config);
        singletonChart = chart;
        toggleButtonWellness(this);
        if (chart.config.data.datasets[0].hidden) {
          chart.config.data.datasets[0].hidden = false;
          selectedValue.push("fatigue");
        } else {
          chart.config.data.datasets[0].hidden = true;
          // https://stackoverflow.com/questions/5767325/how-do-i-remove-a-particular-element-from-an-array-in-javascript
          var index = selectedValue.indexOf("fatigue");
          if (index > -1) {
            selectedValue.splice(index, 1);
          }
        }
        chart.update();

      });
    //display soreness on the graph
    $("#Soreness").click(function() {
        if (singletonChart != null) {
          singletonChart.destroy();
        }
        ctx = document.getElementById("canvasTeamGraph");
        chart = new Chart(ctx, config);
        singletonChart = chart;
        toggleButtonWellness(this);
        if (chart.config.data.datasets[1].hidden) {
          chart.config.data.datasets[1].hidden = false;
          selectedValue.push("soreness");
        } else {
          chart.config.data.datasets[1].hidden = true;
          // https://stackoverflow.com/questions/5767325/how-do-i-remove-a-particular-element-from-an-array-in-javascript
          var index = selectedValue.indexOf("soreness");
          if (index > -1) {
            selectedValue.splice(index, 1);
          }
        }
        chart.update();
      });
    //display mood on the graph
    $("#Mood").click(function() {
        if (singletonChart != null) {
          singletonChart.destroy();
        }
        ctx = document.getElementById("canvasTeamGraph");
        chart = new Chart(ctx, config);
        singletonChart = chart;
        toggleButtonWellness(this);
        if (chart.config.data.datasets[2].hidden) {
          chart.config.data.datasets[2].hidden = false;
          selectedValue.push("mood");
        } else {
          chart.config.data.datasets[2].hidden = true;
          // https://stackoverflow.com/questions/5767325/how-do-i-remove-a-particular-element-from-an-array-in-javascript
          var index = selectedValue.indexOf("mood");
          if (index > -1) {
            selectedValue.splice(index, 1);
          }
        }

        chart.update();
      });
    //display sleep quality on the graph
    $("#Sleep").click(function() {
        if (singletonChart != null) {
          singletonChart.destroy();
        }
        ctx = document.getElementById("canvasTeamGraph");
        chart = new Chart(ctx, config);
        singletonChart = chart;
        toggleButtonWellness(this);
        if (chart.config.data.datasets[3].hidden) {
          chart.config.data.datasets[3].hidden = false;
          selectedValue.push("sleep");
        } else {
          chart.config.data.datasets[3].hidden = true;
          // https://stackoverflow.com/questions/5767325/how-do-i-remove-a-particular-element-from-an-array-in-javascript
          var index = selectedValue.indexOf("sleep");
          if (index > -1) {
            selectedValue.splice(index, 1);
          }
        }

        chart.update();
      });
    //display sleep quality on the graph
    $("#Stress").click(function() {
        if (singletonChart != null) {
          singletonChart.destroy();
        }
        ctx = document.getElementById("canvasTeamGraph");
        chart = new Chart(ctx, config);
        singletonChart = chart;
        toggleButtonWellness(this);
        if (chart.config.data.datasets[4].hidden) {
          chart.config.data.datasets[4].hidden = false;
          selectedValue.push("stress");
        } else {
          chart.config.data.datasets[4].hidden = true;
          // https://stackoverflow.com/questions/5767325/how-do-i-remove-a-particular-element-from-an-array-in-javascript
          var index = selectedValue.indexOf("stress");
          if (index > -1) {
            selectedValue.splice(index, 1);
          }
        }

        chart.update();
      });
    //display sleep quality on the graph
    $("#HeartRate").click(function() {
        if (singletonChart != null) {
          singletonChart.destroy();
        }
        ctx = document.getElementById("canvasTeamGraph");
        chart = new Chart(ctx, config);
        singletonChart = chart;
        toggleButtonWellness(this);
        // TODO: Scaling of Y Axis is super wonky here
        if (chart.config.data.datasets[5].hidden) {
          chart.config.data.datasets[5].hidden = false;
          selectedValue.push("heartRate");
        } else {
          chart.config.data.datasets[5].hidden = true;
          // https://stackoverflow.com/questions/5767325/how-do-i-remove-a-particular-element-from-an-array-in-javascript
          var index = selectedValue.indexOf("heartRate");
          if (index > -1) {
            selectedValue.splice(index, 1);
          }
        }

        chart.update();
      });

    //display sleep quality on the graph
    $("#wellnessGrade").click(function() {
        if (singletonChart != null) {
          singletonChart.destroy();
        }
        ctx = document.getElementById("canvasTeamGraph");
        chart = new Chart(ctx, config);
        singletonChart = chart;
        toggleButtonWellness(this);
        // TODO: Scaling of Y Axis is super wonky here
        if (chart.config.data.datasets[6].hidden) {
          chart.config.data.datasets[6].hidden = false;
          selectedValue.push("wellnessGrade");
        } else {
          chart.config.data.datasets[6].hidden = true;
          // https://stackoverflow.com/questions/5767325/how-do-i-remove-a-particular-element-from-an-array-in-javascript
          var index = selectedValue.indexOf("wellnessGrade");
          if (index > -1) {
            selectedValue.splice(index, 1);
          }
        }

        chart.update();
      });
    //display sleep quality on the graph
    $("#mentalReadiness").click(function() {
        if (singletonChart != null) {
          singletonChart.destroy();
        }
        ctx = document.getElementById("canvasTeamGraph");
        chart = new Chart(ctx, config);
        singletonChart = chart;
        toggleButtonWellness(this);
        // TODO: Scaling of Y Axis is super wonky here
        if (chart.config.data.datasets[7].hidden) {
          chart.config.data.datasets[7].hidden = false;
          selectedValue.push("mentalReadiness");
        } else {
          chart.config.data.datasets[7].hidden = true;
          // https://stackoverflow.com/questions/5767325/how-do-i-remove-a-particular-element-from-an-array-in-javascript
          var index = selectedValue.indexOf("mentalReadiness");
          if (index > -1) {
            selectedValue.splice(index, 1);
          }
        }

        chart.update();
      });
    //display sleep quality on the graph
    $("#physicalReadiness").click(function() {
        if (singletonChart != null) {
          singletonChart.destroy();
        }
        ctx = document.getElementById("canvasTeamGraph");
        chart = new Chart(ctx, config);
        singletonChart = chart;
        toggleButtonWellness(this);
        // TODO: Scaling of Y Axis is super wonky here
        if (chart.config.data.datasets[8].hidden) {
          chart.config.data.datasets[8].hidden = false;
          selectedValue.push("physicalReadiness");
        } else {
          chart.config.data.datasets[8].hidden = true;
          // https://stackoverflow.com/questions/5767325/how-do-i-remove-a-particular-element-from-an-array-in-javascript
          var index = selectedValue.indexOf("physicalReadiness");
          if (index > -1) {
            selectedValue.splice(index, 1);
          }
        }

        chart.update();
      });
  });
}

// format to key format
// https://stackoverflow.com/questions/3552461/how-to-format-a-javascript-date
function formatDate(date) {
  var day = date.getDate();
  var monthIndex = date.getMonth() + 1; // zero based index
  var year = date.getFullYear();

  if (day < 10) {
    day = "0" + day;
  }

  if (monthIndex < 10) {
    monthIndex = "0" + monthIndex;
  }

  return monthIndex + '-' + day + '-' + year;
}

// Changes the background of a date picker button for the graph when clicked
function toggleButtonDateRange(selectedDateRange, button){
  var dateRanges = ["seven", "thirty", "sixty", "ninety"];
  for (var i = 0; i < dateRanges.length; i++) {
    // set all of the other button's backgrounds back to unselected color
    if(dateRanges[i] !== selectedDateRange){
      $('#'+ dateRanges[i] ).removeClass("selectedDateRangeButton");
    }
  }
  // Grab our selected button in jquery and add a class
  $('#'+button.id).addClass("selectedDateRangeButton");
}

// Changes the background of a wellness state button for the graph when clicked
function toggleButtonWellness(button){
  // Grab our selected button in jquery and add a class
  $('#'+button.id).toggleClass("selectedDateRangeButton");
}
