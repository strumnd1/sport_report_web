var coachesTeamCode;
var daysOfTheWeek = ["sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday"];
var coachesEmail;
var coachesName;

$(document).ready( function() {

  // Disables all functionality until JS is fully loaded
  disableFunctionality();

  checkIfLoggedIn()
  .then(function() {
    setupChangePasswordLogic();
    return getTeamCode();
  })
  .then(function(teamCode) {
    coachesTeamCode = teamCode;
    $('#currentSeasonsCodeField').append(coachesTeamCode);

    // Insert coachName into HTML
    if(coachesName){
      $('#coachName').html("Coach " + coachesName);
    }

    // Now that we have the teamcode, continue on with setting everything else up
    createRPEAndWQSchedulerClickListeners();

    readInSchedulerFromFirebase()
    .then(function(){
      reenableDayOfWeekCheckboxes();
    });

    setupStartNewSeasonButtonLogic();
    $('#startNewSeasonButton').attr('disabled',false);

    pullInPlayersForRemoveList()
    .then(function(){
      $('#removePlayerButton').attr('disabled',false);
      $('#playerDropdown').attr('disabled',false);
    });

  })
  .catch(function(err){
    console.log(err);
    if(err === "No team code exists for this coach"){
      // Still let the user change password and stuff, but disable other features that rely on a teamCode
      disableFunctionality();
    }
  });

});

function checkIfLoggedIn(){
  // Setup the log out button
  document.getElementById("tempLogoutButton").addEventListener("click", function(){
    firebase.auth().signOut()
  });
  return new Promise(function (resolve, reject) {
    //establish if the user is there
    firebase.auth().onAuthStateChanged(firebaseUser => {
      // Check if the user is logged in or not
      if(firebaseUser){
        // Make the page visible since they are logged in
        $("#body").removeAttr("hidden");
        uid = firebase.auth().currentUser.uid;
        // Also save what our email is at this part.
        coachesEmail = firebase.auth().currentUser.email;
        return resolve();
      }else{
        // The user logged out
        // Clear sessionStorage and redirect them to login screen
        sessionStorage.clear();
        location.replace("../SignUp/index.html");
        return reject("User is not logged in");
      }
    });
  });
}

function getTeamCode(){
  return new Promise(function (resolve, reject) {
    try {
      if(typeof(Storage) !== "undefined") {

        // First check if they have a coachName stored in sessionStorage
        // If they don't, we are going to want to retrieve it from firebase
        if(sessionStorage.coachesName){
          coachesName = sessionStorage.coachesName;
        }else{
          throw "no coachName";
        }

        // Then check if they have a teamCode stored in sessionStorage
        if(sessionStorage.teamCode){
          return resolve(sessionStorage.teamCode);
        }else{
          throw "no teamcode";
        }
      }else {
        console.log("Sorry, your browser does not support web storage...");
        throw "no teamcode";
      }
    }catch(e){
      // Our session didn't have the teamCode
      // Will have to manually retrieve the teamcode from firebase
      var uid = firebase.auth().currentUser.uid;
      const retrieveTeamCode = firebase.database().ref("coaches").child(firebase.auth().currentUser.uid);
      retrieveTeamCode.once('value').then(function(snapshot){
        var coachInfo = snapshot.val();
        if(coachInfo !== null){
          if(coachInfo.hasOwnProperty('name')){
            var lastName = coachInfo['name'].split(" ").pop();
            if(typeof(Storage) !== "undefined") {
              sessionStorage.coachesName = lastName;
            }
            coachesName = lastName
          }else{
            console.log("settings.js :: getTeamCode :: No coach name exists");
          }

          if(coachInfo.hasOwnProperty('team')){
            if(typeof(Storage) !== "undefined") {
              sessionStorage.teamCode = coachInfo['team'];
            }
            return resolve(coachInfo['team']);
          }
        }
        return reject("No team code exists for this coach");
      });
    }
  });
}

function setupStartNewSeasonButtonLogic(){

  // Change text in startNewSeason modal
  $('#instructionsLabelStartNewSeason').html("Enter your current password for " + coachesEmail + " before continuing:");

  // Click listener for when the Start New Season Button is clicked
  $('#startNewSeasonButton').on('click', function(e) {
    $('#startNewSeasonModal').modal();
  });

  // Click listener for when the continue button in start new season button is clicked
  $('#continueButton').on('click', function(e) {
    // authenticate their password before continuing
    firebase.auth().currentUser.reauthenticateWithCredential(firebase.auth.EmailAuthProvider.credential(firebase.auth().currentUser.email, $("#password").val()))
    .then(function() {
      // They entered the right password. Continue on with the process
      $("#password").val(""); // Clear password out of text field
      $('#continueButton').off('click'); // Removes click listener for continueButton
      // Change id's of buttons back to what they should be
      $('#continueButton').attr("id","startButton");
      $("#password").attr("id","teamName");

      // Change text of the modal to fit new instructions
      $('#startButton').html("Start new season");
      $('#startNewSeasonModalInfoText').html("Password sucessfully verified");
      $('#instructionsLabelStartNewSeason').html("Enter a team name for your new season")
      $('#teamName').attr("placeholder","Grove City Men's Soccer 2018");
      $('#teamName').attr("type","text");

      $('#startButton').on('click', function(e) {
        let teamName = $("#teamName").val();
        if(teamName){
          startNewSeason(teamName);
        }
        else{
          // They didn't give a team name.
          $('#teamName').attr("placeholder","You must enter a team name to continue.")
        }
      });
    })
    .catch(function(err){
      // They entered the wrong password
      $("#password").val("");
      $('#password').attr("placeholder","That password was incorrect. Try again.")
    });
  });
}

function startNewSeason(newTeamName){
  // Make sure the user is connected to the internet before continuing
  var connectedRef = firebase.database().ref(".info/connected");
  connectedRef.once("value").then(function(snap) {
    if (snap.val() === true) {
      // Then we have an internet connection
      // Retrieve all players that are under that specific teamcode from firebase
      const retrievePlayersUnderTeamCode = firebase.database().ref("teams").child(coachesTeamCode).child("players");
      retrievePlayersUnderTeamCode.once('value').then(function(snapshot){

        var players = [];
        // add each UID of each player to an array
        // Get a reference to the players branch of the database
        const playersBranch = firebase.database().ref();

        var updates = {};
        // delete the teamCode branch for each player that was on that team
        for(var key in snapshot.val()){
          updates['/players/'+key+'/teams/'+coachesTeamCode] = null; // setting value to null deletes the key
        }

        // Delete the /teams/coachesTeamCode branch
        updates['/teams/'+coachesTeamCode] = null;

        // First, generate the new team code
        var newTeamCode = makeTeamId();

        // replace the /coaches/coach_uid/team value with our newly generated teamCode
        updates['/coaches/'+firebase.auth().currentUser.uid+'/team'] = newTeamCode;

        // Generate a new teamcode branch under teams
        var week = {
          sunday: false,
          monday: false,
          tuesday: false,
          wednesday: false,
          thursday: false,
          friday: false,
          saturday: false
        };
        var notificationsStruct = {
          WQ: week,
          RPE: week
        };
        var newTeamData = {
          numPlayers: 0,
          teamName: newTeamName,
          notifications: notificationsStruct
        };
        updates['/teams/'+newTeamCode] = newTeamData;

        playersBranch.update(updates)
        .then(function() {
          coachesTeamCode = newTeamCode;
          if(typeof(Storage) !== "undefined") {
            sessionStorage.teamCode = coachesTeamCode;
          }
          // Close the Modal
          $("#teamName").val("");
          $('#startNewSeasonModal').modal('toggle');
          console.log("settings.js :: startNewSeason :: Start new season successful")

          // Reload the webpage so that changes must come into effect
          // 'true' will force the page to reload from the server. 'false' will reload from cache, if available
          window.location.reload(true);
        })
        .catch(function(err){
          console.log(err);
          console.log("settings.js :: startNewSeason :: Start new season failed")
        });

      });
    } else {
      // No internet Connection
      console.log("settings.js :: startNewSeason :: No internet connectivity")
    }
  });
}

function makeTeamId() {
  var text = "";
  let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for (var i = 0; i < 6; i++)
  text += possible.charAt(Math.floor(Math.random() * possible.length));

  return text;
}

// Reads in from firebase what current reccurences we have setup
function readInSchedulerFromFirebase(){
  return new Promise(function (resolve, reject) {
  const retrieveNotifications = firebase.database().ref("teams").child(coachesTeamCode).child("notifications");
  retrieveNotifications.on('value', function(snapshot){
    if(snapshot.hasChild("WQ")){
      for (var key in snapshot.child("WQ").val()){
        //console.log(key);
        $('#weekday-'+ key.substring(0,3) +'-wq').prop('checked', snapshot.child("WQ").val()[key]);
      }
    }
    if(snapshot.hasChild("RPE")){
      for (var key in snapshot.child("RPE").val()){
        //console.log(key);
        $('#weekday-'+ key.substring(0,3) +'-rpe').prop('checked', snapshot.child("RPE").val()[key]);
      }
    }
    return resolve();
  });
});
}

// Sets up change password button and modal behavior
function setupChangePasswordLogic(){
  // Click listener for when the Change Password Button is clicked
  $('#changePasswordButton').on('click', function(e) {
    // Sends the user an email with password reset instructions
    resetPassword();
  });

  // Detects when Change password modal appears
  $('#changePasswordModal').on('show.bs.modal', function (event) {
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    // Modal instructions: https://getbootstrap.com/docs/4.0/components/modal/
    var modal = $(this)
    modal.find('.modal-body').text('Instructions have been sent to ' + userEmail + '. Please follow the link there for further directions. ');
  })

}

// Triggered when the user clicks on the "change password" button
function resetPassword() {
  userEmail = firebase.auth().currentUser.email;
  if(userEmail != null){
    console.log("Settings.js :: resetPassword() :: reset password email sent.");
    firebase.auth().sendPasswordResetEmail(userEmail);
    $('#changePasswordModal').modal();
  }
}

// updates the teamcode/notification/... branch on firebase to whatever the coach just clicked on
// typeOfQuestionnaire can be WQ or RPE
// state will be enabled or disabled.
// ex: if enabled, monday will be set to true in firebase
function updateNotification(typeOfQuestionnaire, dayOfTheWeek, state){
  // Now that we have the teameCode, let's update the database
  var notificationsBranch = firebase.database().ref("teams").child(coachesTeamCode).child("notifications");
  if(typeOfQuestionnaire === "WQ"){
    // establish a reference to the database
    notificationsBranch = notificationsBranch.child("WQ");
  }else{
    // typeOfQuestionnaire must be an RPE
    // establish a reference to the database
    notificationsBranch = notificationsBranch.child("RPE");
  }
  var postData = {
    [dayOfTheWeek]: state
  };
  // Make sure the user is connected to the internet before pushing this update
  var connectedRef = firebase.database().ref(".info/connected");
  connectedRef.once("value").then(function(snap) {
    if (snap.val() === true) {
      // Write the new data to the database
      notificationsBranch.update(postData)
    } else {
      // Something went wrong and that schedule never got updated in the database
      // undo whatever the user did to the checkbox
      $('#weekday-'+ dayOfTheWeek.substring(0,3) +'-'+typeOfQuestionnaire.toLowerCase()).prop('checked', !state);
    }
  });
}

// Creates the click listener for all of the checkboxes for each Scheduler
function createRPEAndWQSchedulerClickListeners() {
  for (var i = 0; i < daysOfTheWeek.length; i++) {
    // Scoping issue fixed with https://stackoverflow.com/questions/19586137/addeventlistener-using-for-loop-and-passing-values
    (function () {
      // WQ
      $('#weekday-'+ daysOfTheWeek[i].substring(0,3) +'-wq').change(function() {
        updateNotification("WQ", $('#'+this.id).attr("data"), this.checked);
      });
      // RPE
      $('#weekday-'+ daysOfTheWeek[i].substring(0,3) +'-rpe').change(function() {
        updateNotification("RPE", $('#'+this.id).attr("data"), this.checked);
      });
    }()); // immediate invocation
  }
}

function pullInPlayersForRemoveList() {

  // Click listener for when the remove player Button in settings page is clicked
  $('#removePlayerButton').on('click', function(e) {
    var playerIDToBeRemoved = $('#playerDropdown').find(":selected").val()

    // Only continue if we actually had a player selected
    if(playerIDToBeRemoved){
      $('#removeAPlayerModal').modal();
      // customize remove button in the model
      $('#removePlayerModalButton').html("Remove " + $('#playerDropdown').find(":selected").text());
      $('#instructionsLabelRemovePlayerModal').html("Enter your current password for " + coachesEmail + " before removing:")
    }
  });

  // Click listener for remove player button in the modal
  $('#removePlayerModalButton').on('click', function(e) {
    // authenticate their password before continuing
    firebase.auth().currentUser.reauthenticateWithCredential(firebase.auth.EmailAuthProvider.credential(firebase.auth().currentUser.email, $("#passwordRemovePlayerModal").val()))
    .then(function() {
      var playerIDToBeRemoved = $('#playerDropdown').find(":selected").val()
      // Removes that player from the team
      removePlayerByID(playerIDToBeRemoved);
    })
    .catch(function(err){
      // They entered the wrong password
      $("#passwordRemovePlayerModal").val("");
      $('#passwordRemovePlayerModal').attr("placeholder","That password was incorrect. Try again.")
    });
  });

  return new Promise(function (resolve, reject) {
  const retrievePlayersForThatTeam = firebase.database().ref("teams").child(coachesTeamCode).child("players");
  retrievePlayersForThatTeam.once('value').then(function(snapshot){
    var players = snapshot.val();
    if(players !== null){
      for (var key in players){
        if(players[key]['playerIdentifier']){
          //console.log(players[key]['playerIdentifier'])
          $('#playerDropdown')
          .append($('<option>', { value : key })
          .text(players[key]['playerIdentifier']));
        }
        else{
          // Then that user didn't have a playerIdentifier for some reason.
          // Display their UID instead
          $('#playerDropdown')
          .append($('<option>', { value : key })
          .text(key));
        }
      }
      return resolve();
    }
  });
});
}

function removePlayerByID(playerID){
  // Get a reference to the database
  const firebaseRef = firebase.database().ref();

  var updates = {};

  // Remove that teamCode from that player's player branch
  updates['/players/'+playerID+'/teams/'+coachesTeamCode] = null; // setting value to null deletes the key

  // Remove that player from the players array under teams branch
  updates['/teams/'+coachesTeamCode+'/players/'+playerID] = null;

  firebaseRef.update(updates)
  .then(function() {
    // Successfully removed player.
    // Remove that player from the select drop down
    $("option[value=" + playerID + "]").each(function() {
      $(this).remove();
    });

    // Close the Modal
    $("#passwordRemovePlayerModal").val("");
    $('#removeAPlayerModal').modal('toggle');
  })
  .catch(function(err){
    console.log(err);
    console.log("settings.js :: removePlayerByID :: Removing player failed")
  });
}

// Disable all schedulers and startNewSeason button
function disableFunctionality(){
  for (var i = 0; i < daysOfTheWeek.length; i++) {
    $('#weekday-'+ daysOfTheWeek[i].substring(0,3) +'-wq').attr('disabled',"true");
    $('#weekday-'+ daysOfTheWeek[i].substring(0,3) +'-rpe').attr('disabled',"true");
  }
  $('#startNewSeasonButton').attr('disabled',"true");
  $('#removePlayerButton').attr('disabled',"true");
  $('#playerDropdown').attr('disabled',"true");
}

function reenableDayOfWeekCheckboxes(){
  for (var i = 0; i < daysOfTheWeek.length; i++) {
    $('#weekday-'+ daysOfTheWeek[i].substring(0,3) +'-wq').attr('disabled',false);
    $('#weekday-'+ daysOfTheWeek[i].substring(0,3) +'-rpe').attr('disabled',false);
  }
}
