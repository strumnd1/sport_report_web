var coachesTeamCode;
var dates = [];
var datesToGraph = new Map();
var finalHolding = [];
var coachesName;
var numDays;
var numWeeks;


$(document).ready( function() {

  //https://stackoverflow.com/questions/14969467/how-to-check-with-jquery-if-any-form-is-submitted
    //need delegates!
  $("#accordion").on("click", ".rpeBtn", function(e){
  //$("#accordion").submit(function (e) {
      e.preventDefault();
      let formId = this.id;  // "this" is a reference to the submitted form
      let weekId = formId[0];
      let selectId = weekId+"select";
      let textId = weekId+"textForm";
      let selectVal = $("#"+selectId).val();
      let textVal = parseFloat($("#"+textId).val());
      let week = finalHolding[weekId];
      
      console.log(textVal);
      
      //loop through and find the day
      //console.log(week);
      week.forEach(function(value, key, map) {
          let temp = new Date(key);
          if(temp.getDay() == selectVal){
              //key is now on the correct day
              
              
              //make sure there is an rpe avg before they can insert
              
              if(value != null){
                  //the day exists in the database
                  //check if an rpe was answered for that day
                  if(value.rpe != null){
                      //they have an rpe avg
                      $("#"+textId).val("");
                      console.log(temp.toString());
                      checkIfLoggedIn()
                      .then(function() {
                        return getTeamCode();
                      })
                      .then(function(teamCode) {
                          const database = firebase.database().ref("teams").child(teamCode).child("wellnessData").child(key).child("timePlayed");
                          //team branch
                          
                          value.time = textVal;
                          value.load = value.rpe*textVal;

                          populate(weekId);
                          calculateChronic()
                          database.set(textVal);
                        

                          //push the time given to that day and reload the page(maybe wait a second or two)
                          //window.setTimeout(function(){ populate(weekId) } ,1000);
                          //location.reload(); 
                      });
                  }
              }
              /*const retreiveTeamInformation = firebase.database().ref("teams").child(teamCode).child("wellnessData");

              retreiveTeamInformation.once('value').then(function(snapshot){
                //check if the date is even in the database
                  if(snapshot.child(key).exists()){
                      //it exists
                      //check if the date has an rpe avg.
                      if(snapshot)
                  }
              }*/
            //
          }
      });
    
  });

  checkIfLoggedIn()
  .then(function() {
    return getTeamCode();
  })
  .then(function(teamCode) {
    coachesTeamCode = teamCode;

    // Insert coachName into HTML
    if(coachesName){
      $('#coachName').html("Coach " + coachesName);
    }

    populateData(teamCode);
        //console.log(finalHolding[0]);
        //console.log(finalHolding[3]);
        //console.log(finalHolding[4]);
        //console.log(finalHolding[5]);
        //console.log(finalHolding[6]);

        //we now have the number of weeks now make dictionary of all the
        //days between that range
        //will also need to split the days up into 4 week variables so we
        //can calculate chronic and acute load



   

    // This is where Richie will do stuff after everything checks out


  })
  .catch(function(err){
    console.log(err);
    //if(err === "No team code exists for this coach"){}
    //probably want to disable everything
  });

});

function checkIfLoggedIn(){
  // Setup the log out button
  document.getElementById("tempLogoutButton").addEventListener("click", function(){
    firebase.auth().signOut()
  });
  return new Promise(function (resolve, reject) {
    //establish if the user is there
    firebase.auth().onAuthStateChanged(firebaseUser => {
      // Check if the user is logged in or not
      if(firebaseUser){
        // Make the page visible since they are logged in
        $("#body").removeAttr("hidden");
        uid = firebase.auth().currentUser.uid;
        // Also save what our email is at this part.
        coachesEmail = firebase.auth().currentUser.email;
        return resolve();
      }else{
        // The user logged out
        // Clear sessionStorage and redirect them to login screen
        sessionStorage.clear();
        location.replace("../SignUp/index.html");
        return reject("User is not logged in");
      }
    });
  });
}

function getTeamCode(){
  return new Promise(function (resolve, reject) {
    try {
      if(typeof(Storage) !== "undefined") {

        // First check if they have a coachName stored in sessionStorage
        // If they don't, we are going to want to retrieve it from firebase
        if(sessionStorage.coachesName){
          coachesName = sessionStorage.coachesName;
        }else{
          throw "no coachName";
        }

        // Then check if they have a teamCode stored in sessionStorage
        if(sessionStorage.teamCode){
          return resolve(sessionStorage.teamCode);
        }else{
          throw "no teamcode";
        }
      }else {
        console.log("Sorry, your browser does not support web storage...");
        throw "no teamcode";
      }
    }catch(e){
      // Our session didn't have the teamCode
      // Will have to manually retrieve the teamcode from firebase
      var uid = firebase.auth().currentUser.uid;
      const retrieveTeamCode = firebase.database().ref("coaches").child(firebase.auth().currentUser.uid);
      retrieveTeamCode.once('value').then(function(snapshot){
        var coachInfo = snapshot.val();
        if(coachInfo !== null){
          if(coachInfo.hasOwnProperty('name')){
            var lastName = coachInfo['name'].split(" ").pop();
            if(typeof(Storage) !== "undefined") {
              sessionStorage.coachesName = lastName;
            }
            coachesName = lastName
          }else{
            console.log("rpeScreen.js :: getTeamCode :: No coach name exists");
          }

          if(coachInfo.hasOwnProperty('team')){
            if(typeof(Storage) !== "undefined") {
              sessionStorage.teamCode = coachInfo['team'];
            }
            return resolve(coachInfo['team']);
          }
        }
        return reject("No team code exists for this coach");
      });
    }
  });
}

function populateData(teamCode){
    // retreive team information with new code
    const retreiveTeamInformation = firebase.database().ref("teams").child(teamCode).child("wellnessData");

    retreiveTeamInformation.once('value').then(function(snapshot){
        snapshot.forEach(function(date) {
            dates.push(new Date(date.key));
            //console.log(date.key);
        });
        
        console.log(dates.length)
        
        if(dates.length > 0){
            dates.sort(date_sort_asc);

            for (var i = 0; i < dates.length; i++) {
                //console.log(dates[i].toString());
                //Do something
            }

            let beginning = dates[0];
            let end = dates[dates.length -1 ];

            //console.log(beginning);
            //console.log(end);

            var startForWeekCount;
            var startingMon;
            var endSunday;
            //console.log("break");
            for (var i = 6; i >= 0 ; i--) {
                var tempDate = new Date(beginning);
                tempDate.setDate(beginning.getDate()-i);
                //console.log(tempDate.toString());
                if(tempDate.getDay() == 1){
                    //monday
                  startingMon = tempDate;
                }
                if(tempDate.getDay() == 0){
                    //sunday
                    startForWeekCount = tempDate;
                }
                //Do something

            }
            //console.log("break");
            //find the last sunday
            for (var i = 0; i < 7 ; i++) {
                var tempDate = new Date(end);
                tempDate.setDate(end.getDate()+i);
                //console.log(tempDate.toString());
                if(tempDate.getDay() == 0){//sunday
                    endSunday = tempDate;
                }
                //Do something

            }
            //console.log("break");
            //console.log(startingMon.toString());
            //console.log(endSunday.toString());

            numDays = datediff(startForWeekCount, endSunday);
            numWeeks = datediff(startForWeekCount, endSunday)/7;

            //console.log(numDays);
            //console.log(numWeeks);

            for (var i = 0; i < numDays ; i++) {
                var tempDate = new Date(startingMon);
                tempDate.setDate(startingMon.getDate()+i);

                var displayDate;
                var displayMonth;
                var displayYear;
                var officialDisplayFullDate;

                //need to put it in the proper format
                if(tempDate.getDate() <10){
                    displayDate = "0"+(tempDate.getDate()).toString();
                }
                else{
                    displayDate = (tempDate.getDate()).toString();
                }

                if(tempDate.getMonth() +1 <10){
                    displayMonth = "0"+(tempDate.getMonth()+1).toString();
                }
                else{
                    displayMonth = (tempDate.getMonth()+1).toString();
                }
                displayYear = (tempDate.getFullYear()).toString();
                officialDisplayFullDate = displayMonth+"-"+displayDate+"-"+displayYear;
                //console.log(officialDisplayFullDate);
                datesToGraph.set(officialDisplayFullDate,null);
            }

            var dayCount = 0;
            var weekCount = 0;
            var weekMap = new Map();
            datesToGraph.forEach(function(value, key, map) {
                    var newValue = value;

                    //check to see if values exist in the database
                    if(snapshot.child(key).exists()){
                        //the date is in the database
                        var rpe = null;
                        var time = null;
                        var workload = null;
                        if(snapshot.child(key).child("averages").child("avgRPE").exists()){
                            //an rpe on this date has been taken
                            rpe = snapshot.child(key).child("averages").child("avgRPE").val();
                        }

                        if(snapshot.child(key).child("timePlayed").exists()){
                            //an rpe on this date has been taken
                            time = snapshot.child(key).child("timePlayed").val();
                        }

                        if(snapshot.child(key).child("workload").exists()){
                            //an rpe on this date has been taken
                            workload = snapshot.child(key).child("workload").val();
                        }

                        newValue = {
                            rpe: rpe,
                            time: time,
                            load: workload,
                            //to set later
                            avg: -1
                        };
                    }

                    if((dayCount % (7) == 0 && dayCount != 0) || dayCount == numDays-1){
                        if(dayCount == numDays-1){
                            //get the last week
                            dayCount++;
                            weekMap.set(key,newValue);
                        }
                        //console.log("Here "+key);
                        //start new week

                        let placeHolder = weekMap;

                        finalHolding.push(placeHolder);
                        //calculate the average for the week


                        //console.log(finalHolding[weekCount])
                        weekMap = new Map()
                        weekCount++;
                    }
                    dayCount++
                    weekMap.set(key,newValue);
            });

            for (var i = numWeeks-1; i >= 0; i--) {
                var cardCollapse = "collapse" + i;
                var cardHeading = "heading" + i;
                var canvasID = "canvas" + i;
                var displayWeek = "Week " + (i+1);

                /*var card = "<div class=\"card\"><div class=\"card-header\" id=" + cardHeading +"><h5 class=\"mb-0\"><button class=\"btn btn-link collapsed\" data-toggle=\"collapse\" data-target=#"+ cardCollapse + " aria-expanded='false' aria-controls="+ cardCollapse + ">"+ displayWeek +"</button></h5></div><div id="+ cardCollapse +" class=\"collapse\ aria-labelledby="+ cardHeading +" data-parent=\"#accordion\"><div class=\"card-body\"><table class=\"table table-bordered table-striped\"><thead><tr><th scope=\"col\">Days</th><th scope=\"col\">Mon</th><th scope=\"col\">Tues</th><th scope=\"col\">Wed</th><th scope=\"col\">Thurs</th><th scope=\"col\">Fri</th><th scope=\"col\">Sat</th><th scope=\"col\">Sun</th><th scope=\"col\">Avg.</th></tr></thead><tbody><tr> <th scope=\"row\">Avg RPE</th><td id = '"+i+"monRPE'>--</td><td id = '"+i+"tueRPE'>--</td><td id = '"+i+"wedRPE'>--</td><td id = '"+i+"thursRPE'>--</td><td id = '"+i+"friRPE'>--</td><td id = '"+i+"satRPE'>--</td><td id = '"+i+"sunRPE'>--</td><td id = '"+i+"avgRPE'>--</td></tr><tr><th scope=\"row\">Time</th><td id = '"+i+"monTime'>--</td><td id = '"+i+"tueTime'>--</td><td id = '"+i+"wedTime'>--</td><td id = '"+i+"thursTime'>--</td><td id = '"+i+"friTime'>--</td><td id = '"+i+"satTime'>--</td><td id = '"+i+"sunTime'>--</td><td id = '"+i+"avgTime'>--</td></tr><tr><th scope=\"row\">Workload</th><td id = '"+i+"monLoad'>--</td><td id = '"+i+"tueLoad'>--</td><td id = '"+i+"wedLoad'>--</td><td id = '"+i+"thursLoad'>--</td><td id = '"+i+"friLoad'>--</td><td id = '"+i+"satLoad'>--</td><td id = '"+i+"sunLoad'>--</td><td id = '"+i+"avgLoad'>--</td></tr></tbody></table><div class = \"row\"><div class=\"col-lg-3\"><select id = '"+i+"select'><option selected value =\"-1\">Select Day</option><option value=\"1\">Mon</option><option value=\"2\">Tues</option><option value=\"3\">Wed</option><option value=\"4\">Thurs</option><option value=\"5\">Fri</option><option value=\"6\">Sat</option><option value=\"0\">Sun</option></select></div><div class=\"col-lg-9\"><form id = '"+i+"form' class = 'rpeForm'>Minutes Played: <input id = '"+i+"textForm' type=\"text\" name=\"Minutes Played\" required><input class = 'rpeBtn' id = '"+i+"btn' type=\"submit\"></form></div></div></div></div></div>";
                $("#accordion").append(card);*/

                var card = "<div class=\"card\"><div class=\"card-header\" id=" + cardHeading +"><div class = \"row\"><div class=\"col-lg-4\"><h5 class=\"mb-0\"><button class=\"btn btn-link collapsed\" data-toggle=\"collapse\" data-target=#"+ cardCollapse + " aria-expanded='false' aria-controls="+ cardCollapse + ">"+ displayWeek +"</button></h5></div><div class=\"col-lg-4\"></div><div class=\"col-lg-4 ratioDisplay\"><div id = '"+i+"ratio'></div></div></div></div><div id="+ cardCollapse +" class=\"collapse\ aria-labelledby="+ cardHeading +" data-parent=\"#accordion\"><div class=\"card-body\"><table class=\"table table-bordered table-striped\"><thead><tr><th scope=\"col\">Days</th><th scope=\"col\">Mon</th><th scope=\"col\">Tues</th><th scope=\"col\">Wed</th><th scope=\"col\">Thurs</th><th scope=\"col\">Fri</th><th scope=\"col\">Sat</th><th scope=\"col\">Sun</th><th scope=\"col\">Avg.</th></tr></thead><tbody><tr> <th scope=\"row\">Avg RPE</th><td id = '"+i+"monRPE'>--</td><td id = '"+i+"tueRPE'>--</td><td id = '"+i+"wedRPE'>--</td><td id = '"+i+"thursRPE'>--</td><td id = '"+i+"friRPE'>--</td><td id = '"+i+"satRPE'>--</td><td id = '"+i+"sunRPE'>--</td><td id = '"+i+"avgRPE'>--</td></tr><tr><th scope=\"row\">Time</th><td id = '"+i+"monTime'>--</td><td id = '"+i+"tueTime'>--</td><td id = '"+i+"wedTime'>--</td><td id = '"+i+"thursTime'>--</td><td id = '"+i+"friTime'>--</td><td id = '"+i+"satTime'>--</td><td id = '"+i+"sunTime'>--</td><td id = '"+i+"avgTime'>--</td></tr><tr><th scope=\"row\">Workload</th><td id = '"+i+"monLoad'>--</td><td id = '"+i+"tueLoad'>--</td><td id = '"+i+"wedLoad'>--</td><td id = '"+i+"thursLoad'>--</td><td id = '"+i+"friLoad'>--</td><td id = '"+i+"satLoad'>--</td><td id = '"+i+"sunLoad'>--</td><td id = '"+i+"avgLoad'>--</td></tr></tbody></table><div class = \"row\"><div class=\"col-lg-3\"><select id = '"+i+"select'><option selected value =\"-1\">Select Day</option><option value=\"1\">Mon</option><option value=\"2\">Tues</option><option value=\"3\">Wed</option><option value=\"4\">Thurs</option><option value=\"5\">Fri</option><option value=\"6\">Sat</option><option value=\"0\">Sun</option></select></div><div class=\"col-lg-9\"><form id = '"+i+"form' class = 'rpeForm'>Minutes Played: <input id = '"+i+"textForm' type=\"text\" name=\"Minutes Played\" required><input class = 'rpeBtn' id = '"+i+"btn' type=\"submit\"></form></div></div></div></div></div>";
                $("#accordion").append(card);

                populate(i);


                //calculate chronic/acute load here

            }
            //console.log(finalHolding[0]);
            //console.log(finalHolding[3]);
            //console.log(finalHolding[4]);
            //console.log(finalHolding[5]);
            //console.log(finalHolding[6]);

            //we now have the number of weeks now make dictionary of all the
            //days between that range
            //will also need to split the days up into 4 week variables so we
            //can calculate chronic and acute load

            calculateChronic();
        }
        
        

    });
    
    

}

function populate(i){
    var dayCount = 0;
    var rpeSum = 0;
    var timeSum = 0;
    var loadSum = 0;
    var rpeNum = 0;
    var timeNum = 0;
    var loadNum = 0;
    
    finalHolding[i].forEach(function(value, key, map) {
            if(value != null){
                if(value.rpe != null){
                    if(dayCount == 0){
                        $("#"+i+"monRPE").text((value.rpe).toFixed(2));
                    }
                    else if(dayCount == 1){
                        $("#"+i+"tueRPE").text((value.rpe).toFixed(2));
                    }
                    else if(dayCount == 2){
                        $("#"+i+"wedRPE").text((value.rpe).toFixed(2));
                    }
                    else if(dayCount == 3){
                        $("#"+i+"thursRPE").text((value.rpe).toFixed(2));
                        }
                    else if(dayCount == 4){
                        $("#"+i+"friRPE").text((value.rpe).toFixed(2));
                    }
                    else if(dayCount == 5){
                        $("#"+i+"satRPE").text((value.rpe).toFixed(2));
                    }
                    else if(dayCount == 6){
                        $("#"+i+"sunRPE").text((value.rpe).toFixed(2));
                    }
                    rpeSum += value.rpe;
                    rpeNum++;
                }
                if(value.time != null){
                    if(dayCount == 0){

                        $("#"+i+"monTime").text((value.time).toFixed(2));

                    }
                    else if(dayCount == 1){
                        $("#"+i+"tueTime").text((value.time).toFixed(2));
                    }
                    else if(dayCount == 2){
                        $("#"+i+"wedTime").text((value.time).toFixed(2));

                    }
                    else if(dayCount == 3){
                        $("#"+i+"thursTime").text((value.time).toFixed(2));
                    }
                    else if(dayCount == 4){
                        $("#"+i+"friTime").text((value.time).toFixed(2));
                    }
                    else if(dayCount == 5){
                        $("#"+i+"satTime").text((value.time).toFixed(2));
                    }
                    else if(dayCount == 6){
                        $("#"+i+"sunTime").text((value.time).toFixed(2));
                    }
                    timeSum += value.time;
                    timeNum++;
                }
                if(value.load != null){
                    if(dayCount == 0){
                        $("#"+i+"monLoad").text((value.load).toFixed(2));
                    }
                    else if(dayCount == 1){
                        $("#"+i+"tueLoad").text((value.load).toFixed(2));
                    }
                    else if(dayCount == 2){
                        $("#"+i+"wedLoad").text((value.load).toFixed(2));
                    }
                    else if(dayCount == 3){
                        $("#"+i+"thursLoad").text((value.load).toFixed(2));
                    }
                    else if(dayCount == 4){
                        $("#"+i+"friLoad").text((value.load).toFixed(2));
                    }
                    else if(dayCount == 5){
                        $("#"+i+"satLoad").text((value.load).toFixed(2));
                    }
                    else if(dayCount == 6){
                        $("#"+i+"sunLoad").text((value.load).toFixed(2));
                    }
                    loadSum += value.load
                    loadNum++;
                }
            }

            dayCount++;
    });
    //calculate average
    if(rpeNum != 0){
        $("#"+i+"avgRPE").text((rpeSum/rpeNum).toFixed(2));
    }else{
        $("#"+i+"avgRPE").text("N/A");
    }

    if(timeNum != 0){
        $("#"+i+"avgTime").text((timeSum/timeNum).toFixed(2));
    }
    else{
        $("#"+i+"avgTime").text("N/A");
    }
    
    var avgLoad = 0;

    if(loadNum != 0){
        avgLoad = (loadSum/loadNum);
        $("#"+i+"avgLoad").text((avgLoad).toFixed(2));
    }
    else{
        $("#"+i+"avgLoad").text("N/A");
    }
    
    //set avgLoad values
    finalHolding[i].forEach(function(value, key, map) {
            if(value != null){
                if(avgLoad == 0){
                    value.avg = null;
                }
                else{
                   value.avg = avgLoad; 
                } 
            }
    }); 
    
    
    
    //$("#"+i+"ratio").append("N/A");
    //if it the week is more than 2 we can start calculating work load
    
    
    
    //console.log(finalHolding[i]);
    
}

function calculateChronic(){
    for (var i = numWeeks-1; i >= 0; i--) {
        if(i > 2){
            let x = i-3;
            let y = i-2;
            let z = i-1;
            
            
            //console.log(finalHolding[i].avg);
            var week1 = null;
            var week2 = null;
            var week3 = null;
            var current = null;
            
            finalHolding[x].forEach(function(value, key, map) {
                if(value != null){
                    week1 = value.avg;
                    
                }
            }); 
            
            finalHolding[y].forEach(function(value, key, map) {
                if(value != null){
                    week2 = value.avg;
                    
                }
            }); 
            
            finalHolding[z].forEach(function(value, key, map) {
                if(value != null){
                    week3 = value.avg;
                    
                }
            }); 
            
            finalHolding[i].forEach(function(value, key, map) {
                if(value != null){
                    current = value.avg;
                    
                }
            }); 

            var curVal;

            var sum = 0;
            var count = 0;

            if(week1 != null){
                sum += week1;
                count++;
            }
            if(week2 != null){
                sum += week2;
                count++;
            }
            if(week3 != null){
                sum += week3;
                count++;
            }

            if(current != null){
                curVal = current;
            }
            else
            {
                $("#"+i+"ratio").text("Chronic/Acute Load: N/A");
                return null;
            }



            var prevAvg = 0;

            if(count == 0){
                $("#"+i+"ratio").text("Chronic/Acute Load: N/A");
                return null;
            }
            else{
                prevAvg = sum/count;
            }

            //console.log(week1);
            //console.log(prevAvg);

            if(prevAvg == 0){
                $("#"+i+"ratio").text("Chronic/Acute Load: N/A");
                return null;
            }
            else {
                $("#"+i+"ratio").text("Chronic/Acute Load: "+ (curVal/prevAvg).toFixed(4));
            }
        }
        else{
            $("#"+i+"ratio").text("Chronic/Acute Load: N/A");
        }
    }
    
    
}

let date_sort_asc = function (date1, date2) {
  // This is a comparison function that will result in dates being sorted in
  // ASCENDING order. As you can see, JavaScript's native comparison operators
  // can be used to compare dates. This was news to me.
  if (date1 > date2) return 1;
  if (date1 < date2) return -1;
  return 0;
};

//https://stackoverflow.com/questions/542938/how-do-i-get-the-number-of-days-between-two-dates-in-javascript
function datediff(first, second) {
    // Take the difference between the dates and divide by milliseconds per day.
    // Round to nearest whole number to deal with DST.
    return Math.round((second-first)/(1000*60*60*24));
}
