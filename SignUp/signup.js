$(document).ready( function() {
    $("#loader").hide();
    //this is the way i should do it

    $( "#signUpForm" ).submit(function( event ) {
        event.preventDefault();
        let name = $("#name").val();
        let teamName = $("#teamName").val();
        let email = $("#email").val();
        let password = $("#password").val();
        let confirmPassword = $("#password-confirm").val();
        if(password != confirmPassword){
            //the passwords don't match
            $("#passwordError").text("The passwords do not match.")
        }else{
            //other validations are through firebase
            signup(email, password)
        }
    });
});

//this probably won't work should have the link in the email bring them to the next page

firebase.auth().onAuthStateChanged(firebaseUser => {

    //check if the user is logged in or not
    if(firebaseUser){
        /*console.log("we got here");
        if(firebaseUser.emailVerified){
            //successful login redirect to the main page
            window.location = "../index.html";
        }*/

        //as of right now this means sign up was successful
        makeNewCoach($("#name").val(), $("#teamName").val(), $("#email").val());
        console.log("loading");
        $("#loader").show();
        //wait for firebase to catch up
         window.setTimeout(nextScreen, 2000);
        //maybe throw loading screen
    }else{
        console.log('not logged in');
    }
});

function signup(email, password){
    //inspiration from: https://stackoverflow.com/questions/48122420/firebase-email-verification-web
    // Only make their log in persist through the session. If they close the browser they will be automatically logged out.
    firebase.auth().setPersistence(firebase.auth.Auth.Persistence.SESSION)
    .then(function() {
      // Existing and future Auth states are now persisted in the current
      // session only. Closing the window would clear any existing state even
      // if a user forgets to sign out.
      // ...
      // New sign-in will be persisted with session persistence.
      return firebase.auth().createUserWithEmailAndPassword(email, password);
    })
    .catch(function(error) {
          // Handle Errors here.
          let errorCode = error.code;
          let errorMessage = error.message;
          console.log("Error Code: " + errorCode);
          console.log("Error Message: " + errorMessage);
          // ...

          $("#errorCode").text(errorMessage);
    });
}

//https://stackoverflow.com/questions/1349404/generate-random-string-characters-in-javascript
function makeTeamId() {
  var text = "";
  let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for (var i = 0; i < 6; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));

  return text;
}

function makeNewCoach(name, teamName, email){
    //alert("in func");
    const database = firebase.database();
    let teamID = makeTeamId();
    let uid = firebase.auth().currentUser.uid;

    console.log(teamID);
    console.log(teamName);
    console.log(uid);
    console.log(email);

    var wq = {sunday: false, monday: false, tuesday: false, wednesday: false, thursday: false, friday: false, saturday: false};
    var rpe = {sunday: false, monday: false, tuesday: false, wednesday: false, thursday: false, friday: false, saturday: false};
    var notificationsObject = {RPE: rpe, WQ: wq};

    database.ref('coaches/'+uid).set({
        name : name,
        email: email,
        team: teamID
    });

    //team branch
    database.ref('teams/'+teamID).set({
        teamName: teamName,
        numPlayers: 0,
        notifications: notificationsObject
    });

    //changeDiv(teamID);

    console.log("incomplete");
}

function nextScreen(){
    $("#loader").hide();
    window.location.replace("finishScreen.html");// = "finishScreen.html";
}
