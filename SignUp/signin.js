$(document).ready( function() {
  //this is the way i should do it
  $("#loader").removeAttr("hidden");
  $("#loader").hide();
  checkIfLoggedIn();
  $( "#signinForm" ).submit(function( event ) {
    event.preventDefault();

    let email = $("#email").val();
    let password = $("#password").val();
    //validations are through firebase
    //if validations succeed login
    login(email, password);
  });

});

function checkIfLoggedIn(){
  firebase.auth().onAuthStateChanged(firebaseUser => {
    //check if the user is logged in or not
    if(firebaseUser){
      // That user is successfully signed in
      // Retrive their teamCode from firebase and store it in a session
      if(typeof(Storage) !== "undefined") {
        // get the code from the coach
        const teamCodeDBRef = firebase.database().ref("coaches").child(firebase.auth().currentUser.uid);
        teamCodeDBRef.once('value').then(function(snapshot){
          var coachInfo = snapshot.val();
          if(coachInfo !== null){
            if(coachInfo.hasOwnProperty('name')){
              var lastName = coachInfo['name'].split(" ").pop();
              sessionStorage.coachesName = lastName;
            }else{
              console.log("signin.js :: onAuthStateChanged :: No coach name exists");
            }

            if(coachInfo.hasOwnProperty('team')){
              sessionStorage.teamCode = coachInfo['team'];
              return (coachInfo['team']);
            }
          }
          throw "signin.js :: Could not retrieve team code."
        }).then(function(teamCode){
          //successful login redirect to the main page
          window.location.replace("../index.html");// = "../index.html";
        })
        .catch(function(err){
          // Couldn't retrieve the team code. Still let them sign in though
          console.log(err);
          window.location.replace("../index.html");// = "../index.html";
        });
      }else {
        console.log("Sorry, your browser does not support web storage...");
        // Still redirect them to the login page though.
        window.location.replace("../index.html");// = "../index.html";
      }
    }else{
      console.log('not logged in');
      // If they are not logged in, show them the login page html
      $("#body").removeAttr("hidden");
    }
  });
}

function login(email, password){

  $("#loader").show();

  // Check if remember me check box was checked
  if ($('#rememberMeCheckbox').is(':checked')){
    // If it is checked, sign them in with local persistence
    firebase.auth().setPersistence(firebase.auth.Auth.Persistence.LOCAL)
    .then(function() {
      // Existing and future Auth states are now persisted in the current
      // session only. Closing the window would clear any existing state even
      // if a user forgets to sign out.
      // ...
      // New sign-in will be persisted with session persistence.
      return firebase.auth().signInWithEmailAndPassword(email, password);
    })
    .catch(function(error) {
      // Handle Errors here.
      // Handle Errors here.
      let errorCode = error.code;
      let errorMessage = error.message;
      console.log("Error Code: " + errorCode);
      console.log("Error Message: " + errorMessage);
      // ...

      //maybe go through possible error messages to describe what is going on
      $("#errorCode").text(errorMessage);
      $("#loader").hide();
    });
  }else{
    // Only make their log in persist through the session. If they close the browser they will be automatically logged out.
    firebase.auth().setPersistence(firebase.auth.Auth.Persistence.SESSION)
    .then(function() {
      // Existing and future Auth states are now persisted in the current
      // session only. Closing the window would clear any existing state even
      // if a user forgets to sign out.
      // ...
      // New sign-in will be persisted with session persistence.
      return firebase.auth().signInWithEmailAndPassword(email, password);
    })
    .catch(function(error) {
      // Handle Errors here.
      // Handle Errors here.
      let errorCode = error.code;
      let errorMessage = error.message;
      console.log("Error Code: " + errorCode);
      console.log("Error Message: " + errorMessage);
      // ...

      //maybe go through possible error messages to describe what is going on
      $("#errorCode").text(errorMessage);
      $("#loader").hide();
    });
  }

}
