$(document).ready( function() {

  $( "#forgotpass" ).submit(function( event ) {
    event.preventDefault();

    let email = $("#email").val();

    $('#changePasswordModal').modal();

    if(email){
      firebase.auth().sendPasswordResetEmail(email)
      .then(function() {
      // Password reset email sent.
      $('#exampleModalLabel').html("Password reset email sent");
      $('#modalBody').html("Password reset instructions have been sent to " + email);
      $('#changePasswordModal').modal();
      console.log("forgotpassword.js :: reset password email sent")
      })
      .catch(function(error) {
        // Error occurred. Inspect error.code.
        $('#modalBody').html("" + error);
        $('#exampleModalLabel').html("Error");
        $('#changePasswordModal').modal();
        console.log("forgotpassword.js :: " + error);
      });
    }


  });

});
