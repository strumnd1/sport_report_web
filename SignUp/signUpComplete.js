$(document).ready( function() {
    
    firebase.auth().onAuthStateChanged(function(user) {
      if (user) {
        //if they got here display the code
        const uid= firebase.auth().currentUser.uid;
        const dbRef = firebase.database().ref("coaches").child(uid);
        dbRef.on('value', function(snapshot){   
            $("#codeField").text(snapshot.child("team").val());
        });
      } else {
        // No user is signed in.
      }
    });
    
    $("#finishButton").click(function(){
        window.location.replace("../index.html");// = "../index.html";
    });
    
});

